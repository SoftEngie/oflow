\label{sec:methodology}
\section{Multi-Task Cascaded Convolutional Network}
    In order to compare the optical flow estimation with face detection methods, the MTCNN \cite{mtcnn_original_paper} was used.
    The MTCNN algorithm was used with the help of the following python implementation \cite{mtcnn_implementation}. The output 
    of the MTCNN algorithm is a list of objects. In each object, the following information can be found : 
    \begin{itemize}
        \item The bounding box of the face, composed of 4 values (x, y, width, height).
        \item The confidence of the detection.
        \item The landmarks of the face, composed of 5 tuples representing the position of the landmarks.
    \end{itemize}

    It is to be noted that here, the bounding boxes considered were the ones with a confidence level higher than 0.9. Other
    detections were discarded. In this sequence, face detected are approximatively around 20 pixels for the width and the height.\\

    \begin{figure}[H]
        \centering
        \includegraphics[width=.7\textwidth, height=8cm]{images/output_mtcnn.png}
        \caption{Example of the output of the MTCNN models, here only the bounding boxes are shown on the image. Indeed,
        for this application, only face detection was required. The confidence percentage is also not shown. However,
        since only detections with a confidence level higher than .9 were kept, boxes can be considered as very certain detections.}
        \label{fig:mtcnn}
    \end{figure}


\section{Determination of the optical flow}
The optical flow estimation was obtained from an implementation of the raft model. The implementation was taken from \cite{2021mmflow}. First, two sequential frames are read from memory in RGB format. Even if the raft model\label{raft_model} is known to be a very efficient model, the frames were then resized to 1440x1080. 
This resizing was done to reduce the computational cost of the optical flow estimation and reduce the memory allocation size. After this preprocessing, the sequential frames are then inferred by the model. The optical flow estimation output by the raft model is a 2-D vector field. The different components represent the horizontal and vertical velocities, u and v. Those velocities can be interpreted as pixel displacement per time interval.\\
    
To graphically represent the optical flow, one can use the magnitude of the vector field which can
be described as the following equation:
\begin{equation}
    \label{eq:magnitude}
    mag(x,y) = \sqrt{u^2 + v^2}
\end{equation}
This representation will be further used for multiple purposes. For graphically displaying this magnitude map.
The image is further normalized between 0 and 255 to obtain an 8-bit, more known as a grayscale image.\\

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/magnitude_map_projet_ba2_2-3.png}
    \caption{Example of the magnitude of an optical flow estimation. The darker spot on the image corresponds to higher velocities.
            The magnitude map allows us to see the incredible accuracy of the RAFT model \cite{raft_original_paper} for the optical flow
            estimation.}
    \label{fig:magnitude}
\end{figure}

\section{Segmentation based on optical flow}
Based on the flow field obtained, a method for determining the moving objects and more particularly
in this context, the moving bodies of the students in the background were developed. 
Based on \cite{optical_flow_segmentation_threshold,motion_detection_based_on_segmentation} and thanks to
the exceptionally accurate flow field obtained \ref{raft_model}, a segmentation based on intensity threshold
was used to determine the moving objects. This method is based on the intensity of the optical flow field.
In other words, the magnitude of the velocity field is used \ref{eq:magnitude}. Only velocities' magnitude above
a certain threshold are considered as moving objects other values are considered as parts of the background.\\

\begin{equation*}
    \label{eq:threshold}
    S(x, y) = \begin{cases}
        1 \quad (\text{motion pixel}) & \text{if mag(x, y)} > T_{velocity}\\
        0 \quad  (\text{background pixel}) & else
    \end{cases}
\end{equation*}

\vspace{1cm}

where $T_{velocity}$ is the threshold value applied on the magnitude representation of the optical flow and $S(x,y)$ is the motion segmentation image.\\

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/lisa_segmented_arbitrary.png}
    \caption{The motion segmentation on the figure is computed using an arbitrary value for the threshold. In this image, the threshold
            for the motion is .5. Therefore, the motion threshold can be interpreted as keeping the part of the image where an object 
            moves more than .5 pixels per interval of exposure.}
    \label{fig:segmentation-manual-threshold}
\end{figure}
In a first attempt to segment the moving object,  the threshold was arbitrarily set to a certain value. However, the chosen threshold value was only based on subjective and visual observations. Therefore, research for finding optimal threshold value determination has been conducted. The goal was to find a method to objectively determine the threshold value based on the motion properties of the different sequences.\\

\subsection{Threshold value determination}
As mentioned in the previous section, the threshold value was arbitrarily set to a certain value. 
In order to find a threshold that would be more accurate in terms of segmenting the moving objects,
a heatmap of the average intensities of the velocities was generated for the whole sequence. 
The heatmap was constructed by computing iteratively the average intensity for each pixel.\\

\begin{equation}
    \label{eq:average_intensity}
    H(x,y)_{n+1} = H(x,y)_{n} + \frac{mag(x,y)_{n+1} - H(x,y)_{n}}{n+1}
\end{equation}
\vspace{1cm}

where H(x,y) is the value of the heat map. Here, it is the average intensity of the pixel (x,y). The term mag(x,y) is the magnitude of the velocity field at the pixel (x,y) and n is the number of frames considered.\\



\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth, height = 11cm]{images/average_map_out.png}
    \caption{Example of a heat motion map for an overall sequence using here the iterative average method. Colors were inverted in this picture to better suits the paper format. This average motion heat map represents the overall motion of the sixth sequence.}
    \label{fig:average_motion_map_example}
\end{figure}

With the heatmap that has been computed for the whole sequence, regions where motion frequently appears are highlighted 
in comparison to regions where motion rarely occurs. Therefore, students that are frequently moving during the sequence
are much more likely to produce a significant halo in the heatmap. Those different zones of interest are then selected from the heatmap and an average over all these regions will be computed. This average will be used as the threshold value. It is to be noted that here it is well the average of all pixels comprised in those regions that were taken and not an average of the averages of each region. This precision is there to insist on the fact that the average is done over a large sample of values and thus the average error is greatly reduced.\\ 

The noise around the persons is due to the fact that motion intensity
between different parts of the background is not homogeneous and the average motion of people is not very much higher than the background average. Therefore, when rescaling the heat map representation, the minimum motion of the background is represented in white. Parts that are less homogeneous are represented with fainting colors and the persons are visible with more saturated colors.\\  

Different properties could be considered to construct a meaningful heat motion map. For example, only the maximum velocities for a whole sequence could be considered. Instead of computing iteratively the average, each frame would be compared, pixel-wise, with the constructed heat map and only the maximum velocities would be kept.
This could be mathematically represented via a formula similar to the iterative average (see \ref{eq:average_intensity}).
\begin{equation}
    \label{eq:maximum_intensity}
    H(x,y)_{n+1} = max(H(x,y)_{n}, mag(x,y)_{n+1})
\end{equation}

The maximum properties compared to the mean properties tends to better highlight region with people's (see Figure \ref{fig:max_motion_example}) motion because they are more likely to produce a very high intensity of motion through the entire sequence. Therefore, the maximum motion heat map contains much more differentiable motion objects, at least visually. However, this method presents several inconveniences. Firstly, the motion of moving objects in the maximum motion heat map is not representative of the motion of usual moving objects. This means that a threshold value based on the maximum would be a value much higher relative to the average motion heat map. Another inconvenience is that every motion on the map is kept and thus a trail of people moving around the room is kept. Lastly, the maximum property may produce regions of motion that are not representative of the entire sequence. An object in the room that will be moved only during a few frames in a zone where no people are present will still be represented on the maximum motion heat map and will not be smoothed out as with the average property.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/max_motion_map_example.png}
    \caption{Example of a heat motion map for an overall sequence using here the maximum method. Colors were inverted in this picture to better suits the paper format. This average motion heat map represents the overall motion of the second sequence. }
    \label{fig:max_motion_example}
\end{figure}

\section{Motion edge extraction}
To further extract information on the moving objects, one may be interested in the discontinuities of the velocity field. Indeed,
in the context of optical flow, discontinuities may be interpreted as the edges of moving objects.
Ideally, a simple edge detector on the intensity representation of the optical flow would be sufficient. However, in real-life 
scenarios, moving objects are not always perfectly separated from each other. In our case, the most common scenario that applies
is the superposition of students' motion on the image. Therefore, on the magnitude image, students cannot really be separated 
easily from each other even if they do not move at the same speed or even in the same direction.\\

Contrary to \cite{motion_edge_computation}, the edges were not directly extracted from the optical flow determination problems
but from the previously computed optical flow field. Since the optical flow field is a 2-D vector field, an approach to analyzing 
the behavior of the 2D vector field would be to use derivative operators.\\

% describe what curl and div measure
% use site https://activecalculus.org/vector/S_Vector_Curl.html
The divergence of a vector field in two dimensions could be interpreted as a measure of the variation of intensities inside the optical flow field. In the context of optical flow, these variations of intensities can be observed between the moving objects and the background. Moreover, the divergence will highlight only the edges of moving objects because the magnitude inside of moving objects tends to be more homogeneous in comparison to the difference between the foreground and the background. The curl operator on the other hand is a measure of the strength of the rotational part of the velocity field. With optical flow, as with the divergence, the curl will highlight the edges of moving objects. 
%how did you compute the curl and div

In order to compute the divergence and the curl, the technique developed has been strongly inspired by the image processing field. Indeed, to efficiently compute the divergence and the curl in a discrete two-dimensional field, the central difference definition of the derivative was used for a small neighborhood.\\

\begin{equation}
    \label{eq:central_difference}
    \frac{\partial f}{\partial x} = \frac{f(x+1,y) - f(x-1,y)}{2}
\end{equation}

This representation of the derivative can be represented as a filter and then used as the kernel of a convolution.
The result of the convolution can then be interpreted as the derivative of the two-dimensional field with the correct filter applied on the right channel of the vector field ($\frac{\partial}{\partial x}$ filter for the x coordinate and 
$\frac{\partial}{\partial y}$ filter for the y coordinate).\\ 


\begin{figure}[H]
    \centering

    \begin{subfigure}{.4\textwidth}
        \centering
        \includegraphics[width=.5\textwidth]{images/x_derivative_filter.png}
        \caption{$\frac{\partial}{\partial x}$ filter}
    \end{subfigure}%
    %\hfill
    \begin{subfigure}{.4\textwidth}
        \centering
        \includegraphics[width=.5\textwidth]{images/y_derivative_filter.png}
        \caption{$\frac{\partial}{\partial y}$ filter}
    \end{subfigure}

    
    \caption{Graphical representation of the applied filter for computing the different derivative operators
    on the velocity field.}
    \label{fig:my_label}
\end{figure}


\section{Evaluation metrics}
\label{sec:evaluation_metric_met}
To assess how successful adding the information of motion is, a method to compare the face detection results with the results of the
motion segmentation was developed. face detection problems and generally object detection problems use the intersection over union
metric to evaluate the performance of the detection\cite{iou_face_detection,iou_and_fp}. For two bounding boxes $B_{pred}$ and $B_{gt}$ 
with the coordinates $(x^1_{pred}, y^1_{pred}, x^2_{pred}, y^2_{pred})$ and $(x^1_{gt}, y^1_{gt}, x^2_{gt}, y^2_{gt})$,
respectively the bounding box predicted and the ground truth bounding box, the bounding box coordinates representing their intersection 
can be computed as followed.\\

\begin{equation}
    x^1_{inter} = max(x^1_{pred}, x^1_{gt}) 
\end{equation}

\begin{equation}
    y^1_{inter} = max(y^1_{pred}, y^1_{gt})
\end{equation}

\begin{equation}
    x^2_{inter} = min(x^2_{pred}, x^2_{gt}) 
\end{equation}

\begin{equation}
    y^2_{inter} = min(y^2_{pred}, y^2_{gt}) 
\end{equation}

where $x^1$ and $y^1$ are the coordinates of the top left corner of the bounding box and $x^2$ and $y^2$ 
are the coordinates of the bottom right corner of the bounding box. It is then easy to compute the three areas 
of the bounding boxes $A_{pred}$, $A_{gt}$ and $A_{inter}$.\\

\begin{figure}[H]
    \centering
    \includegraphics[scale=.7]{images/iou_schema.png}
    \caption{Illustration of the intersection over union metric, the intersection box is highlighted in red.
    The four points of interest are defining the two opposites corner of the intersection box}
    \label{fig:iou}
\end{figure}

The intersection over union metric can be computed with the information of the three different boxes, the ground
truth box, the prediction box, and the intersection with, respectively, an area of $A_{gt}$, $A_{pred}$, and $A_{inter}$. The different parameters of the intersection over union computation are more graphically represented on the figure \ref{fig:iou}.\\

With these parameters, the intersection over union can then be simply computed as the area of the intersection divided by the sum of the area of the prediction box and the ground truth box minus the intersection area.\\
\begin{equation}
    \label{eq:iou}
    IoU = \frac{A_{inter}}{A_{pred} + A_{gt} - A_{inter}}
\end{equation}

\vspace{1cm}

Therefore, if the prediction completely overlaps the ground truth then the $A_{inter}$ would be equal to $A_{gt}$ and to $A_{pred}$. Thus, the IoU would be equal to 1. Inversely, if there is no overlapping between the two bounding boxes then the $A_{inter}$ would be equal to zero and so would be the IoU.\\

For multiple predicted bounding boxes and multiple ground truth bounding boxes,
the intersection over union metric is computed for each pair of bounding boxes.
From the 2D array of IoU values, the maximum IoU value is then selected for each predicted bounding box.
If the maximum IoU value is greater than a threshold (here 0.5), the predicted bounding box is considered as a true positive.
In face detection problems, the true positive corresponds thus to successful face detection. The false positive corresponds
to a predicted bounding box that does not have a maximum IoU value greater than the threshold or in other words the false positive
corresponds to a wrong detection. The false negative corresponds to undetected ground truth, in other words, where the model failed to 
detect a face. The true negative would then correspond to every part left of the image where no faces were detected. This metric is thus not 
very helpful and is thus ignored. Those different cases are graphically represented in the following figure (see figure \ref{fig:cf_mtcnn_explained}). \\

\begin{figure}[H]
    \centering
    \includegraphics[width=.8\textwidth]{images/cases_detection_explained.png}
    \caption{Different cases of the confusion matrix explained for a general object detection problem. As the legend indicates, the ground truth box is in blue, and the predictions are in yellow. True negative is not a case of interest in object detection.
            }
    \label{fig:cf_mtcnn_explained}
\end{figure}


\label{morphological_operation}
Regarding motion segmentation, it was decided to apply the same principle as for the face detection. Firstly, morphological operations are applied to the binary mask obtained from the segmentation. The morphological operations applied consist of two consecutive erosions followed by two consecutive dilatations with a rectangular kernel of shape 5x5 pixels. Those morphological operations ensure that small not meaningful object are removed. This refinement of the motion segmentation mask try to prevents the increase in false positives.\\

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/morphological_operations.png}
    \caption{Effect of the aforementioned morphological operations. On the left, the raw binary segmentation mask can be seen. Multiple smalls artifact can be observed.
    After the morphological operations, the different small errors are removed thanks to the erosions operations while the rest of the objects are intact due to the dilation operations.}
    \label{fig:my_label}
\end{figure}
The intersection over union metric, used for object detection, is not adapted to the motion segmentation problem. Indeed, the motion segmentation obtained 
does not provide bounding boxes of the faces but rather a mask of the whole body. Therefore, the intersection over union is replaced
by a metric that looks at the number of pixels that are contained inside the ground truth bounding box. Since, the segmentation is
only isolating moving objects, and in the case of this work only objects to be detected move in this sequence, a valid segmentation is one 
that has some pixels inside the bounding box. A segmentation is considered a correct detection if the contained number of pixels is greater than zero and is a maximum
for all the others predictions. The meaning of the true positives, false positives, false negatives, and true negatives are 
explained in the following figure (see figure \ref{fig:cf_segme_explained}) .\\

\begin{figure}[H]
    \centering
    \includegraphics[width=.8\textwidth]{images/cases_segmentation_explained.png}
    \caption{Different cases of the confusion matrix explained for the specific object motion segmentation. Motion segmentation is considered correct whenever pixels of the binary masks are inside of a ground truth box.
            }
    \label{fig:cf_segme_explained}
\end{figure}

As with object detection cases, the true negative is not very helpful and is also ignored. Another important distinction is that the true positive 
in the case of the segmentation is not an accurate enough classification for correct segmentation. Indeed, three cases might still lead to a true positive.
There is the case where the object to be detected is too much segmented and thus multiple objects are correct segmentation for a single ground truth box, 
this case will be referred as a sur-segmentation. There is also the opposite case where the object to be detected is not segmented enough and thus the segmentation
overlaps on multiple ground truth boxes, this case will be referred to as a sub-segmentation. Finally, there is the case where the object is well segmented 
and there is only one object for one ground truth box, this case will be referred to as a correct segmentation (see Figure \ref{fig:correct_segmenation}).\\


\begin{figure}[H]
    \centering

    \begin{subfigure}{0.3\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=5cm]{images/sursegmented_frame1.png}
        \caption{}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.3\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=5cm]{images/sursegmented_frame2.png}
        \caption{}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.3\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=5cm]{images/sursegmented_mask.png}
        \caption{}
    \end{subfigure}
    
    \caption{Illustration of the sur-segmentation case. On the figure, a) represents the first frame, b) is the following frame and finally c) is the motion mask obtained thanks to the optical flow estimation. The sur-segmentation can be seen here because the person is split in multiple blobs}
    \label{fig:sursegmentation_case}
\end{figure}

\begin{figure}[H]
    \centering

    \begin{subfigure}{0.3\textwidth}
        \centering
        \includegraphics[width=\textwidth]{images/undersegmented_frame1.png}
        \caption{}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.3\textwidth}
        \centering
        \includegraphics[width=\textwidth]{images/undersegmented_frame2.png}
        \caption{}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.3\textwidth}
        \centering
        \includegraphics[width=\textwidth]{images/undersegmented_mask.png}
        \caption{}
    \end{subfigure}
    
    \caption{Illustration of the sub-segmentation case. In the figures, a) represents the first frame, b) is the following frame; and finally c) is the motion mask obtained thanks to the optical flow estimation. The sub-segmentation here is the agglomeration of the 2 persons moving that will produce only one resulting blob}
    \label{fig:subsegmentation_case}
\end{figure}

\begin{figure}[H]
    \centering

    \begin{subfigure}{0.3\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=5cm]{images/correct_segmented_frame1.png}
        \caption{}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.3\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=5cm]{images/correct_segmented_frame2.png}
        \caption{}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.3\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=5cm]{images/correct_segmented_mask.png}
        \caption{}
    \end{subfigure}
    
    \caption{Illustration of the correct-segmentation case. On the figure, a) represents the first frame, b) is the following frame and finally c) is the motion mask obtained thanks to the optical flow estimation. The correct segmentation produce an unique blob for a person. Ideally the segmentation would have occurred for the head of the person. However, this constraint may be too imposing since it would require that people moves their head.}
    \label{fig:correct_segmenation}
\end{figure}




\label{sec:randd}

\section{Optical flow results}
 % add an introductory text
    Different methods for computing the optical flow have been tested on the dataset. For determining, the most suitable
    method for our application, two Characteristics were considered. The first one is the computation time of the optical flow
    and the second one is the accuracy of the optical flow. The RAFT model was chosen based on those two characteristics.

\subsection{Optical flow computation time}
    As seen, in previous sections, many approaches to optical flow estimation are possible. 
    In an optimal scenario, the computation complexity of the optical flow estimation should be as low as possible while 
    the accuracy of the estimation should be as high as possible. Firstly, the computation time of a classical approach
    to optical flow estimation was benchmarked. In comparison, the computation time of the raft model\cite{raft_original_paper}
    was benchmarked on the same CPU and with an GPU acceleration. The raft model is recently known as the fastest model
    for data-driven optical flow by far \cite{optical_flow_scene_flow}. The specifications of the hardware used for the benchmarking 
    can be found in the following Table \ref{tab:hardware}.
    
    \begin{table}[h]
        \centering
            \begin{tabular}{|c|c|}
            \hline  
                \textbf{Hardware} & \textbf{Specifications} \\ \hline
                CPU & AMD Ryzen 7 3700X 8-Core Processor @ 3.60GHz \\ \hline
                GPU & NVIDIA GeForce RTX 2060 \\ \hline
                RAM & 16 GB \\ \hline
                VRAM & 6 GB \\ \hline
                \end{tabular}
        \caption{Hardware specifications}
        \label{tab:hardware}
    \end{table}

    The comparison of the computational cost can be seen by observing the proposed table (see Table \ref{tab:benchmark}).

\begin{table}[H]
    \centering
    \resizebox{\textwidth}{!}{%
    \begin{tabular}{|c|cccc|cccc|cccc|}
    \hline
    \multirow{2}{*}{Sequence} & \multicolumn{4}{c|}{Classical} & \multicolumn{4}{c|}{Raft GPU} & \multicolumn{4}{c|}{Raft CPU}    \\
                              & mean   & std   & min   & max   & mean  & std   & min   & max   & mean   & std   & min    & max    \\ \hline
    first sequence            & 0.309  & 0.010 & 0.281 & 0.339 & 1.985 & 0.379 & 1.656 & 6.628 & 35.407 & 0.895 & 33.299 & 39.619 \\ \hline
    second sequence           & 0.311  & 0.007 & 0.293 & 0.370 & 1.976 & 0.064 & 1.857 & 2.112 & 33.358 & 0.615 & 32.545 & 36.004 \\ \hline
    third sequence            & 0.310  & 0.005 & 0.297 & 0.334 & 1.979 & 0.071 & 1.853 & 2.102 & 34.018 & 1.225 & 32.617 & 37.632 \\ \hline
    fourth sequence           & 0.312  & 0.009 & 0.296 & 0.324 & 1.968 & 0.063 & 1.852 & 2.103 & 33.400 & 0.972 & 32.107 & 36.965 \\ \hline
    fifth sequence            & 0.313  & 0.006 & 0.292 & 0.362 & 1.965 & 0.064 & 1.852 & 2.108 & 33.159 & 0.279 & 32.728 & 34.112 \\ \hline
    sixth sequence            & 0.312  & 0.010 & 0.293 & 0.356 & 1.972 & 0.070 & 1.844 & 2.097 & 34.361 & 0.778 & 32.947 & 37.738 \\ \hline
    \end{tabular}%
    }
    \caption{Benchmark results. The methods were performed on all frames of all sequences of the dataset. }
    \label{tab:benchmark}
\end{table}

    From the benchmark results, it is clear that the classical method is a much faster approach to optical flow estimation.
    It is one order of magnitude higher than the raft model on GPU and almost two orders of magnitude higher on CPU. It is also
    to be noted that here the optical flow is computed on a 1440x1080 image which is rather a large image to be processed
    by any kind of convolutional neural network. Another point of focus is the comparison between the computation complexity
    of the optical flow estimation with the acquisition time of pictures. Indeed, if the acquisition is much longer than The
    computation time of the most computational heavy optical flow estimation then one may consider negligible the computational
    time of the optical flow estimation and use the more accurate approach. This hypothetical scenario is particularly relevant
    in the context of people counting in auditoriums. Indeed, for this application, the level of detail and the size of 
    the scene required to capture every person in the auditorium should be high. Therefore, the acquisition time of the 
    pictures should be as well very high.\\
    
\subsection{Optical flow accuracy}
    To evaluate the accuracy of an optical flow estimation, the most general approach is to compute the endpoint error (EPE)
    \cite{flownet_original_paper} which is simply the Euclidean distance between the ground truth and the estimated optical flow.
    This metric would allow the comparison of the accuracy of the different models and hence it would allow the 
    determination of which model is the most suitable for a segmentation task. However, the EPE requires a ground truth for
    the motion estimation. Due to the extreme difficulty of creating a ground truth for a complex scene, the EPE was thus not 
    available for the proposed dataset. Nevertheless, to give an idea of the accuracy of the two models, the comparison
    of the two models is presented on the well-known KITTI dataset \cite{kitti_dataset_2015}. The metric used to compare the classical method and the raft model by the KITTI dataset is the Fl (Flow) score. The Fl score refers to the percentage of pixels with a flow disparity larger than 3 pixels or with a magnitude difference bigger than 5 percent, in comparison with the ground truth. 
    \cite{cvpr_2015}. 

    \begin{table}[H]
        \centering
        \begin{tabular}{|c | c | c | c |}
        \hline
            {\bf Model} & {\bf Fl-bg} & {\bf Fl-fg} & {\bf Fl-all}\\ \hline
            Raft & 4.74 \% & 6.87 \% & 5.10 \%\\\hline
            Classical Method & 52.00 \% & 58.56 \% & 53.09 \%\\\hline
            \end{tabular}
        \caption{Comparative results between the raft model and the Farneback method. The comparison was made on the
        KITTI dataset. The Fl-bg corresponds to the Fl score of background pixels and the Fl-fg corresponds to the Fl score for
        foreground pixel. The Fl-all column corresponds then to the overall Fl score.}
        \label{tab:kitti_comparison}
    \end{table}

    It can be here clearly seen that the accuracy of the raft is far better than the accuracy of the traditional 
    approach to optical flow estimation on the KITTI dataset.\\


    Qualitatively, the results of optical flow estimation can be compared on different frames of the dataset. 
    Figure \ref{fig:qualitative_comparison} shows the different optical flow methods applied on two sequential frames in the dataset. The classical method presents a lot of noise. To segment the motion of the people, the classical approach is not suitable. On the contrary,
    the result provided by the raft model is so much accurate that segmentation of the motion can be directly done 
    with very clear results.\\

    
 \begin{figure}[H]
        \centering

        \begin{subfigure}{.8\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/of-raft.png}
            \caption{}
        \end{subfigure}
        \begin{subfigure}{.8\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/of-classical.png}
            \caption{}
        \end{subfigure}
        
        \caption{Comparison of the optical flow estimation qualitatively. a) is the result obtained from the RAFT model while b) is the result obtained with the traditional approach. Darker spots in the image correspond to higher velocity magnitude. The color indicates the angle of the motion. Therefore, the white background is zones where no
        motion was found by the optical flow estimation.}
        \label{fig:qualitative_comparison}
\end{figure}



\section{Segmentation results}

\subsection{Average motion map}
    Based on the result of the raft model, a threshold-based segmentation was performed. However, 
    to determine the value of the threshold, an average motion map for whole sequences in the dataset
    was performed. The following figure shows the average motion map for the whole dataset.\\

    \begin{figure}[H]
        \centering
        \includegraphics[width=\textwidth]{images/average_map_out.png}
        \caption{Average motion map. The noise present in the image is due to a rescaling of the magnitude value.}
        \label{fig:average_motion_map}
    \end{figure}

    \newpage
    
    From this heatmap, blobs of motion can be seen. These blobs would correspond to the small displacement that 
    people make. The average motion value in the region where blobs are visible was chosen. Indeed, the average 
    motion has the advantage to be representative of the motion that needs to be thresholded. Also, the average does smooth out
    the different noises that may occur with the optical flow estimation. Other metrics could be used 
    for representing the motion of the whole sequence.
    For example, if the maximum value of the motion was taken instead of a threshold based on these high values would result 
    in a sur-segmentation of the motion. Indeed, with a map of the maximum motion, these same blobs are very shaped.
    However, even if the blobs have roughly the same location, the motion magnitude is radically different. In conclusion, the maximum motion map produces a heat motion map with 

    \begin{figure}[H]
        \centering
        \includegraphics[width=\textwidth]{images/maximum_map_out.png}
        \caption{Maximum motion map. The motion here is much sharper and no noise seems to appear in the image.
        However, this is due to the fact that the maximum motion of an object to be detected is much higher.}
        \label{fig:maximum_motion_map}
    \end{figure}

\subsection{Threshold value determination}
    Subsequently, on the average motion heat map, different regions of interest in the heat map were chosen. Bounding boxes were created around these regions. Therefore, only the average motion intensity of people moving during the sequence will be considered.\\

    \begin{figure}[H]
        \centering
        \includegraphics[width=\textwidth]{images/max_motion_map_roi_delimited.png}
        \caption{Region of interest of the motion map on the average motion map.}
        \label{fig:roi_of_map}
    \end{figure}

    It can be seen that only the region where the motion blobs were visible are chosen. By taking the average value of all these pixels
    the threshold value was determined. The following table presents the threshold used for each sequence.\\

    \begin{table}[H]
        \centering
        \begin{tabular}{|c|c|c|}
            \hline
            Sequence & Threshold value with average motion & Threshold value with maximum motion \\ \hline
            first sequence  & 0.738 & 87.140 \\ \hline
            second sequence & 0.974 & 43.042\\ \hline
            third sequence  & 0.369 & 11.140\\ \hline
            fourth sequence & 0.361 & 42.208\\ \hline
            fifth sequence  & 0.859 & 39.491\\ \hline
            sixth sequence  & 0.615 & 29.157\\ \hline
            average         & 0.652 & 41.413  \\\hline
        \end{tabular}

        \caption{Threshold value retrieved from the average and the maximum motion map. Threshold values are computed as the average value of all pixels inside of regions of interest.}
        \label{tab:threshold_value}
    \end{table}

    It is to be noted that the threshold value obtained via the average motion map was constantly lower than via the maximum motion map. This is due to the fact that the average motion map is more representative of the motion of the whole sequence. Indeed, the maximum motion map is more sensitive to the noise of the optical flow estimation. The threshold values obtained by analyzing the average motion map are close to the threshold that 
    was arbitrarily chosen on visual segmentation criteria. The threshold values computed via the maximum motion heat map are a lot bigger than those obtained with the average or even the arbitrarily chosen value for the threshold. However, for a threshold, only keeping the maximum motion of moving objects may cause the threshold to be too strong.
    
\subsection{Segmentation results}
    Finally, the segmentation was performed on the magnitude of the optical flow. Giving a binary motion mask of the results. The following figure (see Figure \ref{fig:segmentation_blobs}) show an example of such a segmentation.\\

    \begin{figure}[H]
        \centering
        \includegraphics[width=\textwidth, height=12cm]{images/lisa_segmented_with_threhsold_parameter.png}
        \caption{Segmentation result obtained from the determined threshold value. Here the value of the parameter is set to .738. The segmentation is obtained from the second and third frames of the first sequence.}
        \label{fig:segmentation_blobs}
    \end{figure}

    The obtained mask is then composed of multiple blobs that are considered as the final moving objects. However,
    all segmented objects are not of interest. In this context, only persons should be segmented and considered as 
    valid objects. In real-world applications, however, this problem could be resolved by indicating the probable location of people on the initialization step of a sequence. Therefore, only blobs close to probable locations would be considered. These probable locations could be the seats in the auditoriums. For the segmentation with the maximum threshold value (see Table \ref{tab:threshold_value}), the mask was completely empty for the exact same pair of frames. This result confirms that maximum motion map threshold values are too high for the segmentation purpose.\\

\section{Motion edge extraction results}
    Additionally, to the threshold-based segmentation, the motion edge extractor proved to be successful. By applying the divergence or the curl operator on the velocity field, the motion edges should
    come out as they are locations of the high variation of speed between foreground and background.\\

    

    \begin{figure}[H]
        \centering

        \begin{subfigure}{.8\textwidth}
            \centering
            \includegraphics[width=\textwidth, height=9cm]{images/edge_extraction_div.png}
            \caption{}
            \label{fig:curl_edges}
        \end{subfigure}

        \begin{subfigure}{.8\textwidth}
            \centering
            \includegraphics[width=\textwidth, height=9cm]{images/edge_extraction_curl.png}
            \caption{}
            \label{fig:divergence_edges}
        \end{subfigure}
        
        \caption{Edge extraction using two-dimensional mathematics operator. a) is the edge extraction using the
        divergence operator while b) is the one with the curl operator}
        \label{fig:edge_extractor_operators_comparison}
    \end{figure}

    \newpage
    
    Using the derivative operators proved to be very efficient to extract the edges of the motion. It is to be noted
    that the figures \ref{fig:divergence_edges} and \ref{fig:curl_edges} are very similar to each other. However,
    corners seem to be better detected with the curl operator. A possible explanation could be that the rotational strength
    of the motion are bigger than the variation of speed. Indeed, corners of the motion could be seen as pivot point around which
    the motion is articulated. Those points should present a low magnitude of velocity but a high rotational strength.
    Another example is when people are turning around. Velocity is higher on the point the most distant from the rotation
    center. Thus, certain edges from the motion can be best detected with the curl operator.\\

    For future researches, these edges could be used to further segment the obtained blobs from the threshold 
    operation \ref{fig:segmentation_blobs}. The sub segmented blobs could be further separated using the edges 
    found with the different derivative operators.\\

\section{Comparison between the face detection method and the segmentation method}
    Finally, after comparing the results of the segmentation method with the results of the face detection method,
    multiple confusion matrices were computed.
    
\subsection{MTCNN performance}
    For the MTCNN, the detections were performed on all frames for all sequences. Only predicted objects with a 
    confidence score higher than 0.9 were considered valid detections.
    Other detections were simply discarded.\\

    \begin{figure}[H]
        \centering
        \includegraphics[scale=.5]{images/mtcnn_confusion_matrix.png}
        \caption{Confusion matrix results for the mtcnn method over all sequences}
        \label{fig:confusion_matrix_mtcnn}
    \end{figure}

    \newpage 
    As it can be seen within the confusion matrix, the MTCNN performs quite well. First of all, it is necessary to understand the different cases of the confusion matrix.
    In the case of object detection, two types of error can occur, the false positive and the false negative. As discussed in the 
    methodology section \ref{sec:methodology}, there is the false negative error which is the case when an object is not detected
    by the face detection method. This case is not very problematic. A sequence is composed of a consequent amount of frames.
    Thus, it is very unlikely that a person will not be detected over a long period of time.\\ 
    
    \label{explain_false_negative}
    The other error possible, the false positive error, is more problematic. If a false positive occurs then it means that 
    an object that is not a person will be detected as a person. This will lead to an overestimation of the number of people
    in the auditorium. Furthermore, having a number of false positives relatively comparable to the number of true positives tends to warn the user that whenever a detection
    is made, it is not reliable or at least it has a probability . If the user cannot trust the detection, then even for repeated sequences with similar objects
    it will be difficult to detect the real number of people.\\
    
    Nevertheless, MTCNN does perform extremely well. It has very low false positives (551) compared to the number of true positives (10440). There are a consequent number of 
    false negatives (15521). However, as discussed above, this is not that problematic. These false negatives are not always the same faces
    and thus a face still can be detected over multiple frames. The number of true positives is also very acceptable. The number of 
    true positives is slightly inferior to the false negatives, which means that over the all sequence, almost half of 
    the objects have been detected and, again, the objects detected are not always the same over a sequence. Moreover,
    in the context of this sequence, the persons not facing the camera and the blurred faces were also counted. By essence,
    the face detector can objectively not detect these faces.\\

    
    \subsection{Motion Segmentation Result}
        In the case of motion segmentation, the results were computed for all the segmentation masks. After few morphological operations (see \ref{morphological_operation}), the different blobs were labeled and a bounding box was created around them. After, creating a confusion matrix
        for the blobs detections using the method explained here \ref{sec:evaluation_metric_met}, the following confusion matrix was obtained.\\

        \begin{figure}[H]
            \centering
            \includegraphics[scale=.5]{images/segme_confusion_matrix.png}
            \caption{Confusion matrix for the segmentation method over all sequences}
            \label{fig:confusion_matrix_segmentation}
        \end{figure}

        As it can be seen, the motion segmentation does not perform on the same level as the face detection method.
        The number of true positives is far inferior to the number found with the MTCNN model (5449 with segmentation compared to 10440 with MTCNN). Consequently, the number
        of false negatives is thus very high (20512). However, as previously discussed \ref{explain_false_negative}, this is not the most concerning error. It is to be noted that in certain sequences (see \ref{fig:photogrammetrie_sequence_mtcnn}compared to \ref{fig:photogrammetrie_sequence_segme}), the segmentation method does perform better than the mtcnn.\\

        The number of false positives is not satisfying. Indeed, this number should be as low as possible.
        However, in this case and in comparison to the true positives, the false positive is too high (14185 compared to 5549 for the true positives). A possible explanation for this alarming high number of false positives might be the number of small noises within the segmentation. Whatever the size of the blob is, it is still counted as a false positive if it is not considered as a correct detection. Moreover, for sur-segmentation cases, only one blob of the aggregate is counted as a true positive and the rest of the aggregate is counted as a false positive which may contribute furthermore to the number of false positives. In the context of real-world applications, the number of false positives may be too high and could greatly reduce the interest of the motion analysis.\\
        
        A case that diverges also from the MTCNN  is the true positive condition. True positive does not classify 
        accurately what happens with motion segmentation. Indeed, as discussed in the section \ref{sec:evaluation_metric_met}, 
        the true positive should be further divided into three subclasses: sub-segmented, sur-segmented and correctly 
        segmented. 

        \begin{figure}[H]
            \centering
            \includegraphics[width=\textwidth]{images/true_positive_classification.png}
            \caption{True Positive division into subclasses. In the figure, the sum of the three subclasses are amounting to less than the true positives. This is due to the fact that the sub-segmentation cases are overlapping on multiple ground truth boxes. Therefore, the cases where the sub-segmentation contains different people is only one sub-segmentation case.}
            \label{fig:segme_cases}
        \end{figure}

        It can be seen that the number of correctly segmented true positives dominate the number of true positives.
        The number of sub-segmented detection is coming in second. Finally, sur-segmented detections are the least frequent case. This result is comforting as it shows that the threshold value parameter was correctly chosen. Indeed, the threshold value is the parameter that helps to separate the background from the foreground. If the threshold value is too low, then the different blobs may not be separated anymore and the number of sub-segmented detections will increase. A sub-segmentation is the worst-case scenario for the motion segmentation method. Indeed, if a person is sub-segmented, then the location of the person or persons is really hard to estimate. Therefore, when comparing the segmentation cases \ref{fig:segme_cases}, the number of sur-segmented and correct segmentation are amounting to approximately the double of sub-segmentation cases. It could be argued that sur-segmentation could be a case more concerning than the sub-segmentation. The number of false positives in comparison to the true positives is a crucial factor regarding the reliability of the method. Moreover, sur-segmentation increases the number of false positives. Therefore, sur-segmentation is decreasing the specificity of the model.\\
        
        The different blobs produced by the sur-segmentation could be remerged together using information such as motion contour extraction. In fact, every blob that would be inside the same contour could eventually be merged together. Multiple persons could share the same blob due to the segmentation over the intensity of velocities. However, these multiple persons may not share the same angle of motion. The intensity of velocities may be different for the two persons while remaining higher than the threshold thus combining them into one sub-segmentation. Respectively, the curl and the divergence operator would be able to extract the contours of the different persons under these circumstances.\\


        Nevertheless, when used as the main detector, the motion segmentation method is not reliable enough. Due to the higher number of false positives, a detection made with the motion segmentation is more likely to be a wrong detection than a correct one. However, for future works and real-world applications, a combination of size and shape constraints could be used to improve the motion segmentation method. These constraints would need to be adapted to every sequence. For example, the distance between the acquisition device and the students in the auditorium would radically change the size of the blobs.\\

     \subsection{Combining MTCNN and motion segmentation}
        It has been established that the MTCNN performs better than the motion segmentation method. However,
        MTCNN is not perfect and cases exist for which the face detection method fails. In this case, motion segmentation
        is particularly useful, for example, in cases where a face is blurred. Indeed, blur occurs when the object to be detected is in motion.\\

        By combining the two methods, it means that, firstly, the MTCNN will be applied on the first frame. The correctly detected faces will then be removed from the ground truth set. Subsequently, the motion segmentation method will be applied to the same frame and its next frame. The Results of the segmentation will then be evaluated on the remaining cases not detected by the MTCNN. Therefore, the objects not detected via the MTCNN and the motion segmentation will be considered as the false negatives set and the detected cases either via mtcnn or, subsequently, via the motion segmentation will be the true positives set.

        \begin{figure}[H]
            \centering
            \includegraphics[width=\textwidth]{images/combining_method.png}
            \caption{Procedure of the two methods combination for the evaluation on the ground truth dataset.}
            \label{fig:combining_method_schema}
        \end{figure}
        
        
        
        The correctly segmented persons will then increase the number of true positives and in parallel decrease the number of false negatives. Regarding the false positive case, only the false positives produced by the MTCNN were kept due to the very different nature of false positives for each method. However, it is to keep in mind that the false positives of the segmentation have not disappeared but were only not shown on the confusion matrix for clarity purposes.\\   

        This approach has the advantage of showing directly the motion segmentation's usefulness as an additional layer of information. Only the different true positives between the methods will be kept. As a result, the number of true positives detected by only the motion segmentation and the number of cases detected by both methods is 
        only a simple difference between the number of true positives found by combining the method with the MTCNN true positives number.\\

        

        \begin{figure}[H]
            \centering
            \includegraphics[width=.7\textwidth]{images/mixed_confusion_matrix.png}
            \caption{Confusion matrix for the combination of the MTCNN and the motion segmentation method}
            \label{fig:confusion_matrix_mixed}
        \end{figure}

        The confusion matrix for the combination of the two methods is shown in the figure above. It can be seen that
        the number of true positives increases in comparison to the MTCNN-standalone method. Also, another advantage
        of using motion segmentation upon the MTCNN, within real-world applications, is the increased confidence in detection whenever a face and a motion are detected close to the same location.\\

        Moreover, the increase of true positives while combining the methods shows that motion segmentation indeed brings
        a useful supplementary layer of information. In fact, if non-detected boxes are left after applying the mtcnn methods and the motion still catches those cases then it clearly indicates that motion analysis is an improvement over a classical detection approach.\\

        Even if, combining methods, increased the number of true positives. The number of correct detections compared to undetected objects is still far from perfect. This ratio is approximatively equal to 52\%. The model does thus not seem sensitive enough to detect much more than half of the objects even when motion information is added. Nevertheless, the increase of true positives due to the motion information (amounting to 2989 detections) is a confirmation that motion is an extra layer of information that could not be inferred via the MTCNN.\\

        It is to be noted that the number of true positives added via the segmentation does not correspond to the number of true positives detected with the motion analysis alone. This indicates that MTCNN and motion segmentation share 2560 correct detections together.\\

        


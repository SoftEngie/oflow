
\section{Context}
    People counting task is a very demanding problem. The non-triviality of the problem is due to multiple factors.
    Persons can be partially occluded. The crowd of interest can be divided between stationary and moving 
    persons\cite{people_counting_indoor}.\\

    More particularly, the task of counting people is nowadays used on campuses for various purposes. A common technique for 
    counting people is to base the counting on unique features of people. The most common feature is the face. Therefore, face detection
    is usually used to count people\cite{people_counting_at_campuses}.\\

    Works on face detection and face alignment problems have  recently received a lot of attention from the scientific community
    \cite{viola_jones_original_paper,mtcnn_original_paper,tiny_face,ZENG2019136}.  However, people counting via face detection requires that the method used is able to detect different scales of faces, in order that the partial occlusion of faces is handled properly. In certain cases, the faces are not frontally oriented, and thus counting people
    can not be done via face detection. Furthermore, a general common point to these methods is the fact that they usually consider different frames as independent of each other. However, in the context of people counting in auditoriums,
    this assumption is not valid. Indeed, more information can be extracted from the video sequence if motion between frames is taken 
    into account.\\

\section{Question Research}
    The aim of this master's thesis is to evaluate \textbf{the benefits of motion analysis on people counting method relying on face detection
    and to which extent this additional layer of information could be used to improve such methods.}

    Therefore, this thesis will discuss the following points :
    \begin{itemize}
        \item How can the motion field be analyzed and which type of information can be retrieved from it?
        \item How does the motion field compares or complements the face detection algorithm?
    \end{itemize}

    This work also aims to provide a methodology to answer these questions. The methodology will be developed 
    in accordance with previous works that have been proposed for relatively similar problems.

\section{Related work}
\subsection{Deep Learning}
    The field of machine learning can be briefly defined as the study of computational methods that improve with collected data\cite{MohriRostamizadehTalwalkar18}. 
    Even though the scope of this thesis cannot cover all the usages of machine learning, it is important to mention that
    machine learning has been extensively used in computer vision applications over the last decade. Problematics such as object
    recognition and detection, object segmentation, and many others have been tackled using machine learning techniques\cite{MohriRostamizadehTalwalkar18}.\\

    Deep learning is a subcategory of machine learning that uses artificial neural networks to solve diverse problems. 
    It is a relatively new field that has exploded over the last few years. Many state-of-the-art solutions are nowadays
    based on deep learning techniques \cite{deep_learning_in_computer_vision}.\\

    \subsection{Convolutional Neural Network}
    In computer vision, a well-known and used network architecture is the convolutional neural network (CNN). From the work of 
    \cite{cnn_original_paper}, CNN models have been applied to various computer vision tasks. The main concept behind a convolutional 
    neural network is to introduce convolutional layers in the network. These layers will apply as their name suggests, a 
    convolution operation on the input they received. However, the kernel used by the layer is where the learning takes place.
    Indeed, the kernel is adjusted during the training phase. Therefore, the layer is trained to extract features of interest.\\

    \begin{figure}[h!]
        \centering
        \includegraphics[width=\textwidth]{images/convolution_operation.png}
        \caption{Illustration of the mechanisms of a convolutional layer. A neighbouring, which has similar dimension to the kernel, is taken around a pixel for which the convolution will be computed. Subsequently, the convolution is computed by a multiplication, element-wise, with the kernel and after that a sum of all elements of the neighborhood.}
        \label{fig:cnn_operation}
    \end{figure}

    Convolution layers are generally followed by pooling layers. The function of these layers is to downsample the result of
    the convolution operation and thus reducing the dimensions of the representation and reducing the number of parameters
    of the network which decreases the computational cost\cite{convolutional_neural_network_review}.\\

    \begin{figure}[H]
        \centering
        \includegraphics[width=\textwidth]{images/convolutional_model.png}
        \caption{Schematic representation of a classical combination of input, convolution and pooling layers. The convolutional and pooling layers are generally repeated until the output is flattened to connect to a fully connected layer. Activation layers are not represented on this diagram for the sake of clarity.}
        \label{fig:cnn_model}
    \end{figure}
    
    
\subsection{MTCNN} 
    Multi-Task Cascaded Convolutional Networks (MTCNN) is a framework introduced by Zhang et al. in 2016\cite{mtcnn_original_paper}.
    It is one of the pioneer works to propose a method that combines the problem of face detection and face alignment
    through the use of convolutional neural networks.\\ 

    The work proposed is decomposed into three stages accomplished separately by three different neural networks : 
    The proposal network (P-Net), the refinement network (R-Net,) and the output network (O-Net). The P-Net stage is charged to 
    propose the different candidates bounding boxes that could correspond to the expected face or landmark. After that, the output of the P-Net is passed onto the R-Net. The second stage is charged with refining
    the results of the first stage. The refinement is done with the removal boxes having a very high level of overlapping using
    a Non-Maximum-Suppression method. The third stage has the responsibility of describing the faces detected thanks to stages two and three
    with five different landmarks (eye right/left, nose, lips extremity right/left) for each bounding box.\\
    \begin{figure}[H]
        \centering
        \includegraphics[scale=.6]{images/mtcnn_architecture.png}
        \caption{Representation of the MTCNN architecture. The Cascade aspect of the network can here be seen on the diagram. Indeed,
        the output of every part of the network is here fed to the next one. The input image is here fed to every different network. The
        P-Net and R-net both produce a set of bounding boxes as the output. The O-net is then charged to add also facial landmarks
        to the different candidates.}
        \label{fig:mtcnn_architecture}
    \end{figure}

    Different methods were tested on the WIDER FACE dataset which is a subset of the well known WIDER dataset. In this dataset, 32.203 images were taken and 393.703 faces were labeled for the purpose of face detection methods comparison. Samples are separated in three categories : easy, medium and hard detection which is based on variations in scale, pose and occlusion.
    The MTCNN framework outperformed the other state-of-the-art methods previously proposed by a large margin\cite{widerface_dataset} on the medium and hard cases and all methods performs similarly on the easy set. In terms of accuracy or precision, the method proposed is significantly higher than the other methods.\\
    
\section{Optical Flow}
    In the analysis of video sequences, one may be interested in the motion of the objects in the scene\cite{optical_flow_review}.
    With such information, one can perform tasks such as object tracking\cite{object_tracking}, object segmentation
    \cite{optical_flow_based_Segmentation_active_contour,optical_flow_segmentation_threshold}.
    Optical flow estimation has always been a challenging task in the field of computer vision. In more than 40 years of research, 
    many techniques and concepts have emerged to solve this problem. Optical flow can be seen as a 2D vector field where each
    point represents the change of brightness in the image. Even though the optical flow is not strictly equal to the motion field, it can
    be seen as a good approximation of it.\\

    The first assumption made to estimate the optical flow is the brightness constancy assumption\cite{optical_flow_review}. 
    This assumption states that during a short interval of time, the brightness of a pixel remains constant.

    \begin{equation}
        I(x,y,t) \approx I(x + \delta x, y + \delta y, t+\delta t)
    \end{equation}
    where $I(x,y,t)$ is the intensity of the pixel at position $(x,y)$ at time $t$.
    if we apply a Taylor expansion to the right side of the equation, we get:
    \begin{equation}
        I(x + \delta x, y + \delta y, t+\delta t) \approx I(x,y,t) + \frac{\partial I}{\partial x} \delta x + \frac{\partial I}{\partial y} \delta y + \frac{\partial I}{\partial t} \delta t
    \end{equation}
    and thanks to the brightness constancy assumption, we can write:
    \begin{equation}
        \frac{\partial I}{\partial x} \delta x + \frac{\partial I}{\partial y} \delta y + \frac{\partial I}{\partial t} \delta t = 0
    \end{equation}
    then if we divide both sides by $\delta t$ we get the following equation which is the optical flow constraint equation \cite{optical_flow_review}:
    \begin{equation}
        \frac{\partial I}{\partial x} u + \frac{\partial I}{\partial y} v + \frac{\partial I}{\partial t} = 0
    \end{equation}
    where $u$ and $v$ are the components of the optical flow vector field and represent the horizontal 
    and vertical velocity of the image respectively.
    
    It can be seen that with only one equation and two unknowns, the optical flow cannot be uniquely determined. This is known
    as the aperture problem. To solve, this problem, more constraints need to be added to the system.\\
    
    \subsection{Traditional method}
    Over the years, different methods for estimating the optical flow and solving the aperture problem have been proposed. Pioneers in the field\cite{HORN1981185,lk_iterative} proposed two methods for optical flow estimation. 
    The first one is the Horn and Schunck method\cite{HORN1981185} which estimate the optical flow by introducing the 
    smoothness constraint. This constraint states that the variations in brightness should be slight. This method adds 
    a global constraint which results in a dense optical flow field that is computed for every pixel.\\
    
    Another approach that has been taken by \cite{lk_iterative}, is to add a local constraint to the system.
    This constraint states that the velocities flow is smooth in a local neighborhood. In other terms, neighboring pixels
    should have approximately the same velocity. This method is known as the Lucas-Kanade method. Therefore, since the method is computed for a few pixels of interest in the image, this type of method is known as sparse optical flow.\\

    It is to be taken into account that the list of methods presented here is not exhaustive. Only the most 
    common methods have been presented here. Over the years, these two methods have received a lot of improvements 
    from the scientific community \cite{combining_lk_horn, horn_improved}.\\

    
    \subsection{Data Driven Optical Flow}
    Relatively recently, a dominating trend for optical flow estimation has been the use of CNN architecture to tackle the
    optical flow problem. More particularly, the U-Net architecture \cite{u_net_original_paper} fits very well the optical flow problem.
    The work proposed by \cite{flownet_original_paper} has introduced a U-Net architecture in the context of optical
    flow estimation. To solve the problem, the research team developed two different encoder parts: FlowNetS and FlowNetC. The FlowNetS architecture takes an input composed of two images stacked together and outputs the optical flow via
    a generic CNN pipeline. Another architecture developed to solve the problem is FlowNetC. Instead of stacking immediately the two
    images, the FlowNetC architecture has two smaller CNN networks that reduce the dimensionality of the inputs before stacking them
    together.

    \begin{figure}[H]
        \centering
        \includegraphics{images/flownets_and_c.png}
        \caption{Graphical representation of FlowNetS and FlowNetC networks architecture.
                 The figure was directly used from the original paper \cite{flownet_original_paper}. 
                 In the FlowNetSimple model, the input is composed of two sequential RGB frames concatenated together. The 6 channels input is then passed through a convolutional network. In comparison, the FlowNetC model takes the two RGB frames as
                 two separate inputs for two similar networks that will merge the two frames at a later stage in a correlation layer. 
                }
        \label{fig:flownet}
    \end{figure}

    The output of the network is then sent to the decoder part. As in a generic U-Net architecture, the decoder is composed 
    of convolutional layers and upsampling layers.\\ 

    This work was a milestone in the field of optical flow estimation. Many others works have been inspired by this work such as
    the FlowNet2.0 network \cite{flownet2_original_paper} which is an iterative network that stacks various FlowNetS and FlowNetC
    networks together. Other works focus on the computational cost of such networks. Indeed, FlowNet2.0 is a very heavy Network 
    with very high computational cost. With this problem in mind, other works proposed lite weight networks compared to FlowNet2, such as \cite{liteflownet,pwc_net,spynet_network}
    The most recent data-driven and also the most performant model proposed to this date and to the best of our knowledge is the RAFT network \cite{raft_original_paper}.\\
    
    The Recurrent All-Pairs Field Transforms network is composed of three components: a feature encoder, a correlation layer, and an update operator\cite{raft_original_paper}.
    This architecture is inspired by the optimization aspect of traditional optical flow methods \cite{HORN1981185}.
    First, the encoder extracts per-pixel features, and the correlation layer evaluates the pattern similarity between pixels.
    The update operator then refines the optical flow as an iterative optimization method would. However, RAFT can learn
    what features are relevant and thus are not predefined by human hands.\\

    Characteristics that make RAFT a novel and efficient approach to optical flow estimation is the high-resolution flow field
    that is maintained throughout the network. Unlike many other works presented here 
    \cite{flownet2_original_paper, pwc_net, spynet_network, liteflownet,flownet_original_paper} that use a low to high resolution approach, 
    RAFT is able to maintain a high-resolution flow field throughout the network which means that it is less prone to miss small objects.

    \begin{figure}[H]
        \centering
        \includegraphics[width=\textwidth]{images/example_output_raft.png}
        \caption{Optical flow estimation example output by the RAFT model. The magnitude of velocities is represented on the image with the saturation of color. The angle of direction for the velocity vector is here represented with colors.}
        \label{fig:example_raft_output}
    \end{figure}

\section{Report Outline}
    In this work, different classic datasets used for the face detection problem and the optical flow problem 
    are presented in section \ref{sec:material}. the different pros and cons of these datasets will be discussed. Subsequently,
    the methods used to study the usefulness of the optical flow problem for the face detection problem are presented 
    \ref{sec:methodology}. Then, the various results obtained from experiments are presented and discussed \ref{sec:randd}.
    Finally, the conclusion of the work is presented \ref{sec:conclusion}.
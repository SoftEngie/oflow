import json
import os
from typing import Tuple
import cv2
import numpy as np
import scipy
import tqdm
from matplotlib import pyplot as plt


class BBox:
    def __init__(self, x_start, y_start, x_end, y_end):
        """
        Constructor for the bbox class. The most left top points and bottom right points are taken
        as information to define the bbox on a 2D space. If the given points are not
        :param x_start: left most coordinate
        :param y_start: top most coordinate
        :param x_end: right most coordinate
        :param y_end: bottom most coordinate
        """
        self.xs = int(x_start)
        self.ys = int(y_start)
        self.xe = int(x_end)
        self.ye = int(y_end)
        self.width = abs(self.xe - self.xs)
        self.height = abs(self.ye - self.ys)

        if self.xs > self.xe:
            temp = self.xs
            self.xs = self.xe
            self.xe = temp

        elif self.ys > self.ye:
            temp = self.ys
            self.ys = self.ye
            self.ye = temp

    def getSPoint(self) -> Tuple[int, int]:
        """
        Get starting point (top left most coordinate)
        :return: starting point
        """
        return int(self.xs), int(self.ys)

    def getEPoint(self) -> Tuple[int, int]:
        """
        Get ending point (bottom right most coordinate)
        :return: getting point
        """
        return int(self.xe), int(self.ye)

    def getArea(self) -> int:
        """
        Compute the area of the bounding box
        :return: bbox area
        """
        return self.width * self.height

    def printBox(self) -> None:
        """
        Print the bbox coordinate at the terminal
        :return:
        """
        print(f"xs : {self.xs}, ys : {self.ys}, xe : {self.xe}, ye : {self.ye}")


class ConfusionMatrix:

    def __init__(self, tp: int = 0, fp: int = 0, fn: int = 0):
        """
        Constructor for the confusion matrix for object detection
        :param tp: initial true positives
        :param fp: initial false positive (wrong detection)
        :param fn: initial false negative (undetected object)
        """
        self.tp = tp
        self.fp = fp
        self.fn = fn
        self.tn = 0

    def update(self, dtp: int, dfp: int, dfn: int, dtn: int = 0) -> None:
        """
        Update the confusion matrix with
        :param dtp: delta true positive
        :param dfp: delta false positive (wrong detection)
        :param dfn: delta false negative (undetected object)
        :param dtn: delta true negative (N/A)
        :return:
        """
        self.tp += dtp
        self.fp += dfp
        self.fn += dfn
        self.tn += dtn

    def save(self, path) -> None:
        """
        Save the confusion matrix result to a file.
        :param path: location where to save the file.
        :return:
        """
        cf = [[self.tp, self.fp],
              [self.fn, self.tn]]

        with open(path, "w") as fp:
            json.dump(cf, fp, indent=4)

    def saveplot(self, path):
        """
        Save the plot of the confusion matrix
        :param path: location where to save the plot
        :return:
        """
        cf = [[int(self.tp), int(self.fn)],
              [int(self.fp), int(self.tn)]]
        mat_con = np.array(cf)
        fig, px = plt.subplots(figsize=(7.5, 7.5))
        px.matshow(mat_con, cmap=plt.cm.YlOrRd, alpha=0.5)
        for m in range(mat_con.shape[0]):
            for n in range(mat_con.shape[1]):
                if n * m != 1:
                    px.text(x=m, y=n, s=mat_con[m, n], va='center', ha='center', size='xx-large')
        px.text(x=1, y=1, s='N/A', va='center', ha='center', size='xx-large')
        plt.xticks([0, 1], [1, 0])
        plt.yticks([0, 1], [1, 0])
        plt.xlabel('Actuals', fontsize=16)
        plt.ylabel('Predictions', fontsize=16)
        plt.savefig(path)


def createJsonResultForMask(maskDir, dirout, seq_start=1, seq_stop=50):
    """
    Create bboxes for a segmentation motion binary mask directory
    :param maskDir: the directory of the binary masks
    :param dirout: path of the save location
    :param seq_start: starting number of the sequence
    :param seq_stop: ending number of the sequence
    :return:
    """
    if not os.path.exists(dirout):
        os.makedirs(dirout)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))

    for j in tqdm.tqdm(range(seq_start, seq_stop), desc=f"{maskDir.split('/')[-2]}"):

        segmented = cv2.imread(f"{maskDir}/{j:04d}-{j + 1:04d}_segmented.png", cv2.IMREAD_GRAYSCALE)
        val, thresh = cv2.threshold(segmented, 127, 255, cv2.THRESH_BINARY)

        opening = cv2.morphologyEx(thresh, cv2.MORPH_ERODE, kernel)
        opening = cv2.morphologyEx(opening, cv2.MORPH_ERODE, kernel)
        opening = cv2.morphologyEx(opening, cv2.MORPH_DILATE, kernel)
        opening = cv2.morphologyEx(opening, cv2.MORPH_DILATE, kernel)

        out = cv2.connectedComponentsWithStats(opening, 8)
        (labels, _, stats, _) = out
        blobs = []
        for i in range(1, labels):
            x = stats[i, cv2.CC_STAT_LEFT]
            y = stats[i, cv2.CC_STAT_TOP]
            w = stats[i, cv2.CC_STAT_WIDTH]
            h = stats[i, cv2.CC_STAT_HEIGHT]
            blob_info = {'box': [int(x), int(y), int(w), int(y)],
                         'segmentation': (opening[y:y + h, x:x + w]).astype(int).tolist()}
            blobs.append(blob_info)

        with open(f"{dirout}/{j:04d}-{j + 1:04d}-blobs.json", "w") as fp:
            json.dump(blobs, fp, indent=4)


def readMTCNNBboxes(json_path):
    """
    Read the Bboxes produced by the mtcnn face detection pipeline
    :param json_path: path to the file location.
    :return:
    """
    with open(json_path, "r") as fp:
        bboxes_info = json.load(fp)
    bboxes = []
    for b in bboxes_info:
        if b['confidence'] > 0.9:
            x, y, width, height = b['box']
            box = BBox(x, y, x + width, y + height)
            bboxes.append(box)
    return bboxes


def readSegBboxes(json_path):
    """
    Read the Bboxes produced by the motion segmentation pipeline
    :param json_path: path to the file location.
    :return:
    """
    with open(json_path, "r") as fp:
        bboxes_info = json.load(fp)
    bboxes = []
    for b in bboxes_info:
        x, y, width, height = b['box']
        box = BBox(x, y, x + width, y + height)
        bboxes.append(box)
    return bboxes


def readGroundTruth(json_path):
    """
    Read the bboxes presents in the ground truth
    :param json_path: json_path: path to the file location.
    :return:
    """
    with open(json_path, "r") as fp:
        bboxes_info = json.load(fp)
    bboxes = []
    for b in bboxes_info['shapes']:
        box = BBox(b['points'][0][0], b['points'][0][1], b['points'][1][0], b['points'][1][1])
        bboxes.append(box)
    return bboxes


def bbox_iou(boxGT, boxPRED) -> float:
    """
    Compute the Intersection Over the Union for a Ground Truth bbox and a Predicted bbox
    :param boxGT: ground truth bbox
    :param boxPRED: prediction bbox
    :return: the computed IoU metric
    """
    intersect_xs = max(boxGT.xs, boxPRED.xs)
    intersect_ys = max(boxGT.ys, boxPRED.ys)
    intersect_xe = min(boxGT.xe, boxPRED.xe)
    intersect_ye = min(boxGT.ye, boxPRED.ye)

    interW = intersect_xe - intersect_xs
    interH = intersect_ye - intersect_ys

    # if the Predicted bbox is inside the bounding then the object is considered as detected
    # This palliate the problem that sometimes the gt bboxes are larger than the predicted bboxes
    if boxGT.xs < boxPRED.xs and boxGT.ys < boxPRED.ys and boxGT.xe > boxPRED.xe and boxGT.ye > boxPRED.ye:
        return 1.0

    # Non overlapping bboxes should not be considered
    if interW <= 0 or interH <= 0:
        return -1.0

    inter_area = interW * interH
    boxA_area = boxGT.getArea()
    boxB_area = boxPRED.getArea()

    iou = inter_area / (boxA_area + boxB_area - inter_area)
    return iou


def bbox_seg_iou(boxGT, boxPRED, seg):
    """
    Compute a metric very much alike the intersection over union. However, here the metric returned
    is the number of segmentation pixels inside a ground truth box
    :param boxGT: bbox of ground truth
    :param boxPRED: bbox of predicted object
    :param seg: binary mask of the whole frame
    :return: the number of pixels contained inside the ground truth
    """
    intersect_xs = max(boxGT.xs, boxPRED.xs)
    intersect_ys = max(boxGT.ys, boxPRED.ys)
    intersect_xe = min(boxGT.xe, boxPRED.xe)
    intersect_ye = min(boxGT.ye, boxPRED.ye)

    interW = intersect_xe - intersect_xs
    interH = intersect_ye - intersect_ys

    # Correction: reject non-overlapping boxes
    if interW <= 0 or interH <= 0:
        return -1.0

    inter_region = seg[intersect_ys: intersect_ye, intersect_xs: intersect_xe]
    pixels_inside = inter_region.sum()
    return pixels_inside


def match_bboxes(bbox_gt, bbox_pred, IOU_THRESH=0.5):
    """
    Find the best match between bboxes based on an IoU metric. If the matched pair have an IoU
    above a certain threshold then the pair is considered as a correctly detected object. If the
    pair is under a certain threshold then it is a false positive (wrong detection). the number
    of false negative is evaluated as the number of ground truth that were not matched to a pair
    and thus remains undetected.
    :param bbox_gt: the set of ground truth bboxes
    :param bbox_pred: the set of predicted bboxes
    :param IOU_THRESH: the Intersection Over Union threshold
    :return: the true positive, false positive, false negative and the index of not selected gt bboxes in the set
    """
    n_true = len(bbox_gt)
    n_pred = len(bbox_pred)
    min_iou = 0.0

    # create a matrix for all pair of gt bbox and pred bbox
    iou_matrix = np.zeros((n_true, n_pred))

    # Complete all the cells of this matrix
    for i in range(n_true):
        for j in range(n_pred):
            iou_matrix[i, j] = bbox_iou(bbox_gt[i], bbox_pred[j])

    # Creating a square matrix for allowing the computation through an hungarian matching.
    # To do this add empty filling rows and cols
    # adding filling rows if they are more predictions
    if n_pred > n_true:
        diff = n_pred - n_true
        iou_matrix = np.concatenate((iou_matrix,
                                     np.full((diff, n_pred), min_iou)),
                                    axis=0)
    # adding filling rows if they are more ground truth
    if n_true > n_pred:
        diff = n_true - n_pred
        iou_matrix = np.concatenate((iou_matrix,
                                     np.full((n_true, diff), min_iou)),
                                    axis=1)

    # Perform an Hungarian matching for resolving the problem of choosing pair of bbox that maximize the iou_matrix
    # score if the iou is comprised between 0.0 and 1.0
    index_ground_truth_choosen, index_predictions_choosen = scipy.optimize.linear_sum_assignment(1 - iou_matrix)

    # remove dummy assignments
    sel_pred = index_predictions_choosen < n_pred
    idx_pred_actual = index_predictions_choosen[sel_pred]
    idx_gt_actual = index_ground_truth_choosen[sel_pred]

    ious_actual = iou_matrix[idx_gt_actual, idx_pred_actual]
    sel_valid = (ious_actual > IOU_THRESH)  # Keep only the IoU above a certain threhold
    sel_fp = (ious_actual > 0)  # Generally the threhsold value is set to .5
    sel_fp = np.logical_xor(sel_valid, sel_fp)
    label_tp = sel_valid.astype(int)
    label_fp = sel_fp.astype(int)

    tp = label_tp.sum()  # tp are the number of pair that are valid
    fp = label_fp.sum()  # fp are the number of detection below the IoU threshold

    indexes = np.arange(n_true)

    # keep the unused bboxes for combining fdetection and motion segmentation
    gt_not_used = np.delete(indexes, idx_gt_actual)

    # false negatives correspond to the number of gt bboxes undetected
    fn = n_true - tp

    return tp, fp, fn, gt_not_used


def match_bboxes_seg(bbox_gt, bbox_pred, seg):
    """
    Find the best match between bboxes based on a maximum of pixels detected. If the matched pair have the
    biggest number of segmented pixels inside the segmentation then the pair is considered
    as a correctly detected object. Also divide further the correct classification inside three sub-categories :
    segmented, oversegmented and undersegmented. The rule for theses sub-categories is as following.
    If an unique object is detected in only one gt bbox then it is correctly segmented. However,
    if an object is inside multiple bboxes then it is under segmented and if multiple objects are inside
    one box then it is over segmented.
    and thus remains undetected.
    :param bbox_gt: the set of ground truth bboxes
    :param bbox_pred: the set of predicted bboxes
    :param seg: the binary mask obtained after the motion segmentation
    :return: the true positive labels, the segmented, oversegmented and undersegmented objects
    """

    n_true = len(bbox_gt)
    n_pred = len(bbox_pred)
    # create a matrix for all pair of gt bbox and pred bbox
    seg_matrix = np.zeros((n_pred, n_true))

    # Complete all the cells of this matrix
    for i in range(n_pred):
        for j in range(n_true):
            seg_matrix[i, j] = bbox_seg_iou(bbox_pred[i], bbox_gt[j], seg)

    gt_labels = np.zeros((n_true,))
    corresponding_seg = np.zeros((n_true,)) - 1
    gt_sursegmented = np.zeros((n_true,))

    # determining oversegmented objects
    for i in range(n_true):
        pr_scores = seg_matrix[:, i]
        if (pr_scores > 0).sum() > 0:
            gt_labels[i] = 1
            corresponding_seg[i] = pr_scores.argmax()
            if (pr_scores > 0).sum() > 1: gt_sursegmented[i] = 1

    pr_subsegmented = np.zeros((n_pred,))

    # determining undersegmented objects
    for i in range(n_pred):
        gt_score = seg_matrix[i, :]
        if (gt_score > 0).sum() > 1: pr_subsegmented[i] = 1

    segmented = 0
    # determining segmented objects
    for i in range(n_true):
        if gt_labels[i] == 1:
            j = int(corresponding_seg[i])
            col = seg_matrix[:, i]
            row = seg_matrix[j, :]
            if (col > 0).sum() == 1 and (row > 0).sum() == 1: segmented += 1

    return gt_labels, segmented, gt_sursegmented.sum(), pr_subsegmented.sum()


def computeResultsForMTCNN(mtcnn_result_path, ground_truth_path, savematrix=False, outpath="mat_iou.json"):
    """
    Compute result for a face detection pipeline
    :param mtcnn_result_path: path to the face detection json file
    :param ground_truth_path: path to the ground truth json file
    :param savematrix: if the confusion matrix should be saved
    :param outpath: the output directory
    :return:
    """
    gt_bboxes = readGroundTruth(ground_truth_path)
    pr_bboxes = readMTCNNBboxes(mtcnn_result_path)
    out = match_bboxes(gt_bboxes, pr_bboxes)
    cf_mat = [[int(out[0]), int(out[1])], [int(out[2]), 0]]
    gt_not_found = out[3]
    result = {'cf_mat': cf_mat, 'gt_left': gt_not_found.tolist()}
    if savematrix:
        with open(outpath, 'w') as fp:
            json.dump(result, fp, indent=4)

    return result


def computeResultsForSegmentation(segmentation_result_path, ground_truth_path, segmentation_image, savematrix=False,
                                  outpath="mat_seg.json"):
    """
    Compute result for a motion detection pipeline
    :param segmentation_result_path: path to the results of the segmentation results
    :param ground_truth_path: path to the results of the ground truth path
    :param segmentation_image: path to the binary mask obtained after the motion segmentation
    :param savematrix: if the matrix should be saved
    :param outpath: path to the output directory
    :return:
    """
    gt_bboxes = readGroundTruth(ground_truth_path)
    pr_bboxes = readSegBboxes(segmentation_result_path)
    segmented_img = cv2.imread(segmentation_image, cv2.IMREAD_GRAYSCALE)
    _, thresh = cv2.threshold(segmented_img, 127, 255, cv2.THRESH_BINARY)

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))

    opening = cv2.morphologyEx(thresh, cv2.MORPH_ERODE, kernel)
    opening = cv2.morphologyEx(opening, cv2.MORPH_ERODE, kernel)
    opening = cv2.morphologyEx(opening, cv2.MORPH_DILATE, kernel)
    opening = cv2.morphologyEx(opening, cv2.MORPH_DILATE, kernel)
    seg = opening
    gt_labels, segmented, oversegmented, undersegmented = match_bboxes_seg(gt_bboxes, pr_bboxes, seg)
    tp = gt_labels.sum()
    fn = len(gt_labels) - tp
    fp = len(pr_bboxes) - tp
    cf_mat = [[int(tp), int(fp)], [int(fn), 0]]
    result = {'cf_mat': cf_mat, 'segmented': segmented, 'sub-segmented': undersegmented, 'sur-segmented': oversegmented}
    if savematrix:
        with open(outpath, 'w') as fp:
            json.dump(result, fp, indent=4)

    return result


def computeResultsForMixed(mtcnn_result_path, segmentation_result_path, ground_truth_path, segmentation_image,
                           savematrix=False, outpath="mat_mixed.json"):
    """
    Compute result for a face detection pipeline followed by a motion detection pipeline
    :param mtcnn_result_path: path to the face detection json file
    :param segmentation_result_path: path to the results of the segmentation results
    :param ground_truth_path: path to the results of the ground truth path
    :param segmentation_image: path to the binary mask obtained after the motion segmentation
    :param savematrix: if the matrix should be saved
    :param outpath: path to the output directory
    :return:
    """
    gt_bboxes = readGroundTruth(ground_truth_path)
    mtcnn_bboxes = readMTCNNBboxes(mtcnn_result_path)
    segme_bboxes = readSegBboxes(segmentation_result_path)
    segmented_img = cv2.imread(segmentation_image, cv2.IMREAD_GRAYSCALE)
    _, thresh = cv2.threshold(segmented_img, 127, 255, cv2.THRESH_BINARY)

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))

    opening = cv2.morphologyEx(thresh, cv2.MORPH_ERODE, kernel)
    opening = cv2.morphologyEx(opening, cv2.MORPH_ERODE, kernel)
    opening = cv2.morphologyEx(opening, cv2.MORPH_DILATE, kernel)
    opening = cv2.morphologyEx(opening, cv2.MORPH_DILATE, kernel)
    out = match_bboxes(gt_bboxes, mtcnn_bboxes)
    cf_mat = [[int(out[0]), int(out[1])], [int(out[2]), 0]]
    gt_not_found = out[3]
    left_boxes = []
    for i in gt_not_found:
        left_boxes.append(gt_bboxes[i])

    gt_labels, segmented, sursegmented, subsegmented = match_bboxes_seg(left_boxes, segme_bboxes, opening)
    tp = gt_labels.sum()
    fn = len(gt_labels) - tp
    fp = len(segme_bboxes) - tp
    cf_mat[0][0] += tp
    cf_mat[1][0] -= tp
    result = {'cf_mat': cf_mat, 'segmented': segmented, 'sub-segmented': subsegmented, 'sur-segmented': sursegmented}

    if savematrix:
        with open(outpath, 'w') as fp:
            json.dump(result, fp, indent=4)
    return result


def computeResultsForSequence(mtcnn_seq, seg_seq, gt_seq, seg_img_dir, outdir, sseq, eseq):
    """
    Compute the confusion matrix for a whole sequence for the face detection, motion segmentation and mixed methods.
    /!\ Important : for now filename should be under the form 04d-out.bboxes.json for face detection bboxes from
    /!\             sseq to eseq and   04d_04d+1-blobs.json and 04d_04d+1_segmented.png for the binary mask
    :param mtcnn_seq: path to the face detection pipeline
    :param seg_seq: path to the motion segmentation pipeline
    :param gt_seq:  path to the ground truth results
    :param seg_img_dir: path to the binary mask
    :param outdir: location to where the results should be saved
    :param sseq: start index sequence
    :param eseq: end index sequence
    :return:
    """
    if not os.path.exists(outdir):
        os.makedirs(outdir)
        os.makedirs(f"{outdir}/mtcnn")
        os.makedirs(f"{outdir}/segme")
        os.makedirs(f"{outdir}/mixed")

    cf_mtccn = ConfusionMatrix()
    cf_seq = ConfusionMatrix()
    cf_combined = ConfusionMatrix()

    segmented = 0
    subsegmented = 0
    sursegmented = 0

    mixed_segmented = 0
    mixed_subsegmented = 0
    mixed_sursegmented = 0

    for i in tqdm.tqdm(range(sseq, eseq)):
        mtcnn_filename = f"{mtcnn_seq}/{i:04d}-out.bboxes.json"
        seg_filename = f"{seg_seq}/{i:04d}-{i + 1:04d}-blobs.json"
        gt_filename = f"{gt_seq}/{i:04d}.json"
        seg_img_filename = f"{seg_img_dir}/{i:04d}-{i + 1:04d}_segmented.png"

        resMtccn = computeResultsForMTCNN(mtcnn_filename, gt_filename, savematrix=True,
                                          outpath=f"{outdir}/mtcnn/{i:04d}.result.json")
        resSegme = computeResultsForSegmentation(seg_filename, gt_filename, seg_img_filename, savematrix=True,
                                                 outpath=f"{outdir}/segme/{i:04d}.result.json")
        resMixed = computeResultsForMixed(mtcnn_filename, seg_filename, gt_filename, seg_img_filename, savematrix=True,
                                          outpath=f"{outdir}/mixed/{i:04d}.result.json")

        segmented += resSegme['segmented']
        subsegmented += resSegme['sub-segmented']
        sursegmented += resSegme['sur-segmented']

        mixed_segmented += resMixed['segmented']
        mixed_subsegmented += resMixed['sub-segmented']
        mixed_sursegmented += resMixed['sur-segmented']

        cf_mtccn.update(resMtccn['cf_mat'][0][0], resMtccn['cf_mat'][0][1], resMtccn['cf_mat'][1][0],
                        resMtccn['cf_mat'][1][1])
        cf_seq.update(resSegme['cf_mat'][0][0], resSegme['cf_mat'][0][1], resSegme['cf_mat'][1][0],
                      resSegme['cf_mat'][1][1])
        cf_combined.update(resMixed['cf_mat'][0][0], resMixed['cf_mat'][0][1], resMixed['cf_mat'][1][0],
                           resMixed['cf_mat'][1][1])

    cf_mtccn.save(f"{outdir}/mtcnn/sequence.result.json")
    cf_mtccn.saveplot(f"{outdir}/mtcnn/sequence_cf.png")

    cf_seq.save(f"{outdir}/segme/sequence.result.json")
    cf_seq.saveplot(f"{outdir}/segme/sequence_cf.png")
    segmented_results = {'segmented': segmented, 'subsegmented': subsegmented, 'sursegmented': sursegmented}
    with open(f"{outdir}/segme/segmented_cat.json", "w") as fp:
        json.dump(segmented_results, fp, indent=4)

    cf_combined.save(f"{outdir}/mixed/sequence.result.json")
    cf_combined.saveplot(f"{outdir}/mixed/sequence_cf.png")
    segmented_results = {'segmented': mixed_segmented, 'subsegmented': mixed_subsegmented,
                         'sursegmented': mixed_sursegmented}
    with open(f"{outdir}/segme/segmented_cat.json", "w") as fp:
        json.dump(segmented_results, fp, indent=4)


if __name__ == '__main__':
    try:
        computeResultsForSequence("path/to/mtcnn_results.json", "path/to/segmentation_results.json",
                                  "path/to/ground_truth.json", "path/to/segmentation_mask.png",
                                  "../resources/output_dir_result_cf_mat", 1, 200)

    except:
        print("path specified are probably wrong")

import json
import os
from typing import Callable

import cv2
import numpy as np
import warnings
from src.oflow_utils import open_frame, get_flow_magnitude, get_flow_angle

try:
    from mmflow.apis import init_model, inference_model
    imported_mmflow = True
except ImportError as error:
    warnings.warn("Could not import the mmflow module. Try to follow the installation guide at : "
                  "https://github.com/open-mmlab/mmflow", ImportWarning)
    imported_mmflow = False

"""
Few variables necessary for the communication with the mmflow library.
Those variables are changeable via the config_mmflow_model function.
The different variables are essential for the oflow_raft_image func.
"""
init_mmflow_raft = False
raft_model = None
config_file = 'raft_8x2_100k_flyingthings3d_400x720.py'
checkpoint_file = 'raft_8x2_100k_flyingthings3d_400x720.pth'
device = 'gpu'


def config_mmflow_model(model_config_file: os.path, model_checkpoint_file: os.path, model_device: str):
    """
    Change the global configuration for the model to be used with the mmflow library.
    :param model_config_file: new configuration file path
    :param model_checkpoint_file: new model checkpoint file path
    :param model_device: is the device to be used with torch (generally either 'gpu' or 'cpu')
    :return:
    """
    global config_file
    global checkpoint_file
    global device
    config_file = model_config_file
    checkpoint_file = model_checkpoint_file
    device = model_device


def oflow_raft(frame1: np.ndarray, frame2: np.ndarray, **kwargs) -> np.ndarray:
    """
    Compute the optical flow for two consecutive frames in time. The model used by default
    is the raft model. However, this can be changed via the config_mmflow_model function. See
    the mmflow documentation for further information : "https://github.com/open-mmlab/mmflow"
    :param frame1: frame at time t
    :param frame2: frame at time t + \delta t
    :return: the resulting optical flow
    """
    try:
        global init_mmflow_raft
        global raft_model
        if init_mmflow_raft == False or raft_model is None:
            raft_model = init_model(config_file, checkpoint_file, device=device)
            init_mmflow_raft = True
        flow = inference_model(raft_model, frame1, frame2)
    except ImportError:
        warnings.warn("MMFlow was not imported, an empty optical flow was returned")
        flow = np.zeros_like(frame1)
    return flow


def oflow_farneback(frame1: np.ndarray, frame2: np.ndarray, **kwargs) -> np.ndarray:
    """
    Compute the optical flow for two consecutive frames in time.
    :param frame1: frame at time t
    :param frame2: frame at time t + \delta t
    :param kwargs: contains the different parameters for a Farneback optical flow computation.
    :key pyr_scale : the scale of the pyramids constructed
    :key levels : nb of pyramids (including original)
    :key winsize : size of the averaging window sliding size
    :key iteration : nb of algorithm iteration
    :key poly_n : size of the neighborhood to find poly expansion
    :key poly_sigma : std for the gaussian filter used to smooth derivative.
    :return: the resulting optical flow
    """
    pyr_scale = kwargs.get("pyr_scale", 0.5)
    levels = kwargs.get("levels", 3)
    winsize = kwargs.get("winsize", 15)
    iterations = kwargs.get("iteration", 3)
    poly_n = kwargs.get("poly_n", 5)
    poly_sigma = kwargs.get("poly_sigma", 1.1)
    flags = cv2.OPTFLOW_FARNEBACK_GAUSSIAN
    frame1 = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)
    frame2 = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)
    out = cv2.calcOpticalFlowFarneback(frame1, frame2, pyr_scale=pyr_scale, levels=levels,
                                       winsize=winsize, iterations=iterations, poly_n=poly_n,
                                       poly_sigma=poly_sigma, flags=flags, flow=None)

    return out


def draw_flow_hsv(flow: np.ndarray) -> np.ndarray:
    """
    Create a HSV representation of the optical flow previously computed.
    The magnitude is represented by the value of the color (from black to color).
    The angle is represented by the color hue. The angle in degrees correspond
    to the hue color cyclic palette.
    :param flow: The optical flow computed
    :return: the HSV representation of the opticalflow but converted to RGB format.
    """
    flowMag = get_flow_magnitude(flow)
    flowAng = get_flow_angle(flow)
    h, w = flowMag.shape[:2]
    hsv = np.zeros((h, w, 3), np.uint8)
    hsv[..., 0] = flowAng * (180 / np.pi / 2)
    hsv[..., 1] = 255
    hsv[..., 2] = cv2.normalize(flowMag, None, 0, 255, cv2.NORM_MINMAX)
    rgb = cv2.cvtColor(hsv, cv2.COLOR_HSV2RGB_FULL)
    return rgb


def add_flow_alpha_to_image(img: np.ndarray, flow: np.ndarray, opacity_level: float = .6) -> np.ndarray:
    """
    Add the optical flow result to the original image with a certain transparency level.
    This superposition is useful for visualizing the result of an optical flow.
    :param img: Original image. (for better superposition use second frame of optical flow)
    :param flow: The optical flow computed
    :param opacity_level: Level of opacity of the optical flow (from 0.0 : invisible to 1.0 : visible)
    :return: the resulting blended image
    """
    flowHsv = draw_flow_hsv(flow)
    flowHsv = cv2.cvtColor(flowHsv, cv2.COLOR_RGB2BGR)
    blend = cv2.addWeighted(img, 1, flowHsv, opacity_level, 0)
    return blend


def get_optical_flow_result(frame1_path: str, frame2_path: str, save_oflow: bool = True, save_img: bool = True,
                            outdir_path: str = "out_oflow", oflow: Callable = oflow_farneback, **kwargs):
    """

    :param frame1_path: the path to the frame 1
    :param frame2_path: the path to the frame 2
    :param save_oflow: determine if a json file should be saved
    :param save_img: save the resulting image
    :param outdir_path: the directory where the result will be saved
    :param oflow: the algorithm for computing the optical flow
    :param kwargs: options for the pipeline
    :return:
    """
    frame1 = open_frame(frame1_path)
    frame2 = open_frame(frame2_path)
    motion_field = oflow(frame1, frame2)
    motion_field_graphic = draw_flow_hsv(motion_field)
    oflow_out_path = kwargs.get("oflow_outpath", frame1_path[:-4] + "_" + frame2_path[:-3] + "flow.json")
    image_out_path = kwargs.get("image_outpath", frame1_path[:-4] + "_" + frame2_path[:-3] + "png")

    if save_img:
        cv2.imwrite(outdir_path + os.path.sep + image_out_path, motion_field_graphic)
    if save_oflow:
        flow_json = {'x': motion_field[:, :, 0].tolist(), 'y': motion_field[:, :, 1].tolist()}
        with open(outdir_path + os.path.sep + oflow_out_path, 'w') as fp:
            json.dump(flow_json, fp, indent=4)
    return motion_field, motion_field_graphic


def run_oflow_on_dir(image_dir: str, output_dir: str, is_numeroted: bool, start_seq: int, end_seq: int) -> None:
    """
    Run the oflow pipeline for a whole directory.
    :param image_dir: the directory containing all the images
    :param output_dir: the directory where results needs to be save
    :param is_numeroted: boolean to see if the images are numeroted?
    :param start_seq: starting index for image sequence
    :param end_seq: ending index for image sequence
    :return:
    """

    if is_numeroted:
        frames_path = [f"{image_dir}{os.path.sep}{i:04d}.jpg" for i in range(start_seq, end_seq)]
    else:
        frames_path = [image_dir + os.path.sep + fp for fp in os.listdir(image_dir) if
                       fp[-4:] == ".png" or fp[-4:] == ".jpg" or fp[-5:] == ".jpeg"]

    for idx, frame_path in enumerate(frames_path):
        if idx < len(frames_path) - 1:
            _ = get_optical_flow_result(frame_path, frames_path[idx + 1], save_bboxes=True, save_img=True,
                                        outdir_path=output_dir)


if __name__ == "__main__":
    of, of_hsv = get_optical_flow_result("../resources/test/0001.jpg", "../resources/test/0002.jpg", save_img=False,
                                         save_oflow=False, oflow=oflow_farneback)

    cv2.imshow("optical flow", of_hsv)
    cv2.waitKey()

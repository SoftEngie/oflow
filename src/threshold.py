import os
from src.oflow_utils import open_flow_from_json, get_flow_magnitude
import numpy as np
import cv2


class MovementMap:

    def __init__(self, w: int, h: int):
        """
        Constructor function for the MovemementMap class.
        :param w: width of the motion map
        :param h: height of the motion map
        """
        self.row = h
        self.col = w
        self._map_x = np.zeros((self.row, self.col))
        self._map_y = np.zeros((self.row, self.col))
        self.iter = 0

    def updateMap(self, flow: np.ndarray):
        """
        Add the value of flow to the current state of the motion map
        :param flow: optical flow state at interval t and t + delta t
        :return:
        """
        self._map_x += flow[..., 0]
        self._map_y += flow[..., 1]
        self.iter += 1

    def getMap(self) -> np.ndarray:
        """
        return the computed Motion Map
        :return: return the computed motion map
        """
        return np.dstack((self._map_x, self._map_y))

    def updateMaxMap(self, flow: np.ndarray):
        """
        Update the motion map taking only the maximum value between current oflow and motion max map
        :param flow: optical flow state at interval t and t + delta t
        :return:
        """
        self._map_x = np.maximum(self._map_x, flow[..., 0])
        self._map_y = np.maximum(self._map_y, flow[..., 1])
        self.iter += 1

    def getNormalizedMap(self):
        """
        Return the average motion map over all the optical flows.
        :return: the average motion map
        """
        avg_x = self._map_x / self.iter
        avg_y = self._map_y / self.iter
        return np.dstack((avg_x, avg_y))


def compute_motion_map(oflow_dir: str, output_dir: str, **kwargs):
    """
    Compute the motion map for an optical flow sequence
    :param oflow_dir: the directory containing all the optical flow previously computed
    :param output_dir: the directory where results needs to be save
    :param kwargs: parameters available for the motion pipeline.
    :return:
    """
    w, h = size = kwargs.get("map_size", (1440, 1080))
    max_map = kwargs.get("max_map", True)
    heatmap = MovementMap(w, h)
    flow_basename_path = os.listdir(oflow_dir)
    flow_path = [oflow_dir + os.sep + flow_name for flow_name in flow_basename_path]
    for oflow in flow_path:
        flow = open_flow_from_json(oflow)
        if max_map:
            heatmap.updateMaxMap(flow)
        else:
            heatmap.updateMap(flow)

    if max_map:
        motion_map = heatmap.getMap()
    else:
        motion_map = heatmap.getNormalizedMap()
    return motion_map


def segment_motion_thresholding(flow: np.ndarray, threshold_val: float = 2.5, quantile_threshold: float = .9,
                                threshOnVal: bool = True) -> np.ndarray:
    """
    Segment motion from a previously computed optical flow with a predefined threshold value
    :param flow: previously computed oflow
    :param threshold_val: threhsold value for the motion segmentation
    :param quantile_threshold: find the threshold value based on a percentile of active velue
    :param threshOnVal: should threshold based on a value or a percentile
    :return: the binary mask for the threshsold
    """
    mag = get_flow_magnitude(flow)
    if not threshOnVal:
        threshold_val = np.quantile(mag, quantile_threshold)
    mask = mag < threshold_val
    return mask


def run_segmentation_on_dir(oflow_dir: str, output_dir: str) -> None:
    """
    Run the motion segmentation pipeline for a whole directory.
    :param oflow_dir: the directory containing all the optical flow previously computed
    :param output_dir: the directory where results needs to be save
    :return:
    """
    flow_basename_path = os.listdir(oflow_dir)
    flow_path = [oflow_dir + os.sep + flow_name for flow_name in flow_basename_path]
    for oflow in flow_path:
        flow = open_flow_from_json(oflow)
        mask = segment_motion_thresholding(flow)
        cv2.imsave(output_dir + os.sep + os.path.basename(oflow)[:-4] + ".png", mask)


if __name__ == "__main__":
    flow = open_flow_from_json("../resources/of_json/0001-0002.flow.json")
    mask = segment_motion_thresholding(flow, threshold_val=1.5).astype(np.uint8)*255
    cv2.imshow("segmentation", mask)
    cv2.waitKey()

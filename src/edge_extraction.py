import json
import os
from typing import Callable
import numpy as np
import cv2

from src.oflow_utils import open_flow_from_json


def convolve2D(signal: np.ndarray, kernel: np.ndarray) -> np.ndarray:
    """
    Convolve the 2 dimensional signal with a kernel.
    :param signal: the 2D signal that will be convolved
    :param kernel: the kernel that will be used for the convolution
    :return: the convoluted signal
    """
    kernel = np.flipud(np.fliplr(kernel))
    conv = cv2.filter2D(signal, -1, kernel)
    return conv


def computeGradient(mag: np.ndarray) -> np.ndarray:
    """
    Compute the gradient intensity for a magnitude representation
    :param mag: the magnitude for which the gradient will be computed
    :return: the gradient intensity of the image
    """
    dx = np.array([[0, 0, 0],
                   [-1, 0, 1],
                   [0, 0, 0]])
    dy = np.array([[0, -1, 0],
                   [0, 0, 0],
                   [0, 1, 0]])
    dm_dx = convolve2D(mag, dx)
    dm_dy = convolve2D(mag, dy)
    return dm_dx + dm_dy


def computeDivergence(flow: np.ndarray) -> np.ndarray:
    """
    Compute the divergence of a 2 dimensional vector field
    :param flow: 2D vector field with two composants (u,v)
    :return: the divergence for the optical flow
    """
    dx = np.array([[0, 0, 0],
                   [-1, 0, 1],
                   [0, 0, 0]])
    dy = np.array([[0, -1, 0],
                   [0, 0, 0],
                   [0, 1, 0]])
    du_dx = convolve2D(flow[:, :, 0], dx)
    dv_dy = convolve2D(flow[:, :, 1], dy)
    div = du_dx + dv_dy
    return div


def computeShear(flow: np.ndarray) -> np.ndarray:
    """
    Compute the shear of a 2 dimensional vector field
    :param flow: 2D vector field with two composants (u,v)
    :return: the shear of the optical flow
    """
    dx = np.array([[0, 0, 0],
                   [-1, 0, 1],
                   [0, 0, 0]])
    dy = np.array([[0, -1, 0],
                   [0, 0, 0],
                   [0, 1, 0]])

    du_dy = convolve2D(flow[:, :, 0], dy)
    dv_dx = convolve2D(flow[:, :, 1], dx)
    shear = du_dy + dv_dx
    return shear


def computeCurl(flow: np.ndarray) -> np.ndarray:
    """
    Compute the curl of a 2 dimensional vector field
    :param flow: 2D vector field with two composant (u,v)
    :return: the curl of the optical flow
    """
    dx = np.array([[0, 0, 0],
                   [-1, 0, 1],
                   [0, 0, 0]])
    dy = np.array([[0, -1, 0],
                   [0, 0, 0],
                   [0, 1, 0]])

    du_dy = convolve2D(flow[:, :, 0], dy)
    dv_dx = convolve2D(flow[:, :, 1], dx)
    curl = dv_dx - du_dy
    return curl


def get_edge_extraction_result(flow: np.ndarray, save_edges: bool = True, save_img: bool = True,
                               outdir_path: str = "out_edges", edge_extractor: Callable = computeCurl,
                               **kwargs) -> np.ndarray:
    """
    Extract Motion edges from a previously computed optical flow
    :param flow: previously computed optical flow taken as a 2D numpy array with two channels (dx and dy)
    :param save_edges: if the edges should be saved as a json file
    :param save_img: if the edges should be saved as a png image
    :param outdir_path: directory to which the image should be saved
    :param edge_extractor: function used for extracting the motion edges
    :param kwargs: options for the motion edge extraction pipeline
    :return:
    """

    motion_edges = edge_extractor(flow)
    edges_out_path = kwargs.get("edges_outpath", "edges_out.json")
    image_out_path = kwargs.get("image_outpath", "edges_out.png")
    if save_img:
        cv2.imwrite(outdir_path + os.path.sep + image_out_path, motion_edges)
    if save_edges:
        edges_json = {'edges': motion_edges.tolist()}
        with open(outdir_path + os.path.sep + edges_out_path, 'w') as fp:
            json.dump(edges_json, fp, indent=4)

    return motion_edges


def run_edges_on_dir(oflow_dir: str, output_dir: str, **kwargs) -> None:
    """
    Run the edge extractor pipeline for a whole directory.
    :param oflow_dir: the directory containing all the optical flow previously computed
    :param output_dir: the directory where results needs to be save
    :return:
    """

    flow_basename_path = os.listdir(oflow_dir)
    edge_extractor = kwargs.get("edge_extractor", computeCurl)
    flow_path = [oflow_dir + os.sep + flow_name for flow_name in flow_basename_path]
    for oflow in flow_path:
        flow = open_flow_from_json(oflow)
        _ = get_edge_extraction_result(flow, edges_outpath=os.path.basename(oflow)[:-4],
                                       image_outpath=os.path.basename(oflow)[:-4],
                                       outdir_path=output_dir, edge_extractor=edge_extractor)


if __name__ == "__main__":
    flow = open_flow_from_json("../resources/of_json/0001-0002.flow.json")
    edges = get_edge_extraction_result(flow, save_edges=False, save_img=False, edge_extractor=computeCurl)
    cv2.imshow("edges", edges)
    cv2.waitKey()

import json
import cv2
import numpy as np


def open_frame(frame_path: str, **kwargs):
    """
    Open a frame with and process the image to get a conform image for face detection.
    :param frame_path: path to the frame to open
    :param kwargs: different parameters available for the open_frame function
    :key size : size for resizing the frame (default is tuple (1440, 1080))
    :key color_cvt : boolean to know if a color conversion RGB to BGR
    :return: the frame in a numpy array format.
    """
    size = kwargs.get("size", (1440, 1080))
    cvt = kwargs.get("color_cvt", True)
    frame = cv2.imread(frame_path)
    if cvt:
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    frame = cv2.resize(frame, size)
    return frame


def open_flow_from_json(json_path) -> np.ndarray:
    """
    Read an optical flow as a json array from the file located at json_path.
    :param json_path: location of the json file containing the optical flow
    :return: the computed optical flow
    """
    with open(json_path) as json_file:
        data = json.load(json_file)
        dx = data['x']
        dy = data['y']
        flow = np.dstack((dx, dy))
    return flow


def get_flow_magnitude(flow: np.ndarray) -> np.ndarray:
    """
    This function computes the magnitude of a given optical flow
    :param flow: a two-dimensional np.ndarray with 2 channels representing the optical flow
    :return: the magnitude of the two dimensional vector
    """
    fx, fy = flow[..., 0], flow[..., 1]
    magnitude = np.sqrt(fx ** 2 + fy ** 2)
    return magnitude


def get_flow_angle(flow: np.ndarray) -> np.ndarray:
    """
    This function computes the angle of a given optical flow. It is to be noted that the
    angle is given in radian and that, to convert it to angle, one should multiply the
    result by 180/pi.
    :param flow: a two-dimensional np.ndarray with 2 channels representing the optical flow
    :return: the angle of the two dimensional vector
    """
    fx, fy = flow[..., 0], flow[..., 1]
    angle = np.arctan2(fy, fx)
    return angle

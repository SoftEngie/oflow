import json
import os
import time
from typing import Callable

import numpy as np

from src.face_detection import detect_faces_mtcnn, open_frame
from src.opticalflow import oflow_raft, oflow_farneback


def oflow_raft_benchmark(frame1, frame2):
    """
    Measure time taken to compute the optical flow with the RAFT Model.
    :param frame1: frame at time t
    :param frame2: frame at time t + \delta t
    :return: time taken in seconds
    """
    start = time.time()
    _ = oflow_raft(frame1, frame2)
    end = time.time()
    return end - start


def oflow_farneback_benchmark(frame1, frame2):
    """
    Measure time taken to compute the optical flow with the Farneback Model.
    :param frame1: frame at time t
    :param frame2: frame at time t + \delta t
    :return: time taken in seconds
    """
    start = time.time()
    _ = oflow_farneback(frame1, frame2)
    end = time.time()
    return end - start


def detect_faces_mtcnn_benchmark(frame):
    """
    Measure time taken to detect the faces with the MTCNN Model.
    :param frame: frame at time t
    :return: time taken in seconds
    """
    start = time.time()
    _ = detect_faces_mtcnn(frame)
    end = time.time()
    return end - start


def create_oflow_benchmark_for_dir(image_dir, start_seq, end_seq,
                                   iteration, oflow_method: Callable, output_file: str):
    """
    Run a benchmark test for a whole directory. In this specific case, the image should have the format 04d and
    be consecutive (0001.jpg, 0002.jpg, ...)
    :param image_dir: directory containing the source images
    :param start_seq: number starting the sequence
    :param end_seq: number ending the sequence
    :param iteration: nb of time a method will be tested on an image.
    :param oflow_method: the optical flow method to be tested
    :param output_file: the file that will contain the result of the benchmark
    :return:
    """
    frames_path = [f"{image_dir}{os.path.sep}{i:04d}.jpg" for i in range(start_seq, end_seq)]
    averages = np.zeros((len(frames_path) - 1,))
    for idx, frame_path in enumerate(frames_path):
        if idx < len(frames_path) - 1:
            results = np.zeros((iteration,))
            time_taken = 0
            for i in range(iteration):
                frame1 = open_frame(frame_path)
                frame2 = open_frame(frames_path[idx + 1])
                time_taken = oflow_method(frame1, frame2)
            results[iteration] = time_taken
            averages[idx] = results.mean()
    with open(output_file, 'w') as fp:
        json.dump(averages.tolist(), fp)


def create_face_detection_benchmark_for_dir(image_dir, start_seq, end_seq, iteration,
                                            fdetection_method: Callable, output_file: str):
    """
    Run a benchmark test for a whole directory. In this specific case, the image should have the format 04d and
    be consecutive (0001.jpg, 0002.jpg, ...)
    :param image_dir: directory containing the source images
    :param start_seq: number starting the sequence
    :param end_seq: number ending the sequence
    :param iteration: nb of time a method will be tested on an image.
    :param fdetection_method: the face detection algorithm to be tested
    :param output_file: the file that will contain the result of the benchmark
    :return:
    """
    frames_path = [f"{image_dir}{os.path.sep}{i:04d}.jpg" for i in range(start_seq, end_seq)]
    averages = np.zeros((len(frames_path) - 1,))
    for idx, frame_path in enumerate(frames_path):
        if idx < len(frames_path) - 1:
            results = np.zeros((iteration,))
            time_taken = 0
            for i in range(iteration):
                frame = open_frame(frame_path)
                time_taken = fdetection_method(frame)
            results[i] = time_taken
            averages[idx] = results.mean()
    with open(output_file, 'w') as fp:
        json.dump(averages.tolist(), fp)


if __name__ == '__main__':
    create_face_detection_benchmark_for_dir("../resources/test", 1, 10, 3, detect_faces_mtcnn_benchmark,
                                            "../benchmark/test_result_mtcnn.json")

    create_oflow_benchmark_for_dir("../resources/test", 1, 10, 3, oflow_farneback_benchmark,
                                   "../benchmark/test_result_oflow.json")

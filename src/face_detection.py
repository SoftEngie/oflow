from typing import List, Callable
import numpy as np
import json
import os
import cv2
import mtcnn

from src.oflow_utils import open_frame


def detect_faces_haar(frame, **kwargs) -> List:
    """
    This function detects faces in an image using the Haar Cascade detector.
    Since the confidence level is not available for the Viola Jones then the confidence is set by default to 1.
    :param frame: image in which faces need to be detected
    :return: list of bounding boxes of detected faces as tuples (x, y, w, h)
    """
    viola_features = kwargs.get("viola_features", '../resources/haarcascade_frontalface_default.xml')
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    face_cascade = cv2.CascadeClassifier(viola_features)
    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=5)
    bboxes = [{'box': (x, y, w, h), 'confidence': 1.0} for (x, y, w, h) in faces]
    return bboxes


def detect_faces_mtcnn(frame, **kwargs) -> List:
    """
    This function detect the faces in an image using the mtcnn library
    :param frame: image in which face need to be detected
    :return: the bboxes information as a dictionaries composed of a 'box' tuple and a 'confidence' value among others.
    """
    detector = mtcnn.MTCNN()
    bboxes = detector.detect_faces(frame)
    return bboxes


def filterDetection(bboxes: List, min_confidence: float = .9) -> List:
    """
    Filter the bounding boxes based on a certain level of confidence.
    :param bboxes: the list of bounding boxes
    :param min_confidence: the minimum confidence value required
    :return: the updated list of bounding boxes with confidence higher than minimum value.
    """
    filtered_bboxes = filter(lambda b: b['confidence'] >= min_confidence, bboxes)
    return list(filtered_bboxes)


def draw_bounding_boxes(frame: np.ndarray, bboxes: List) -> np.ndarray:
    """
    Draw a rectangle at the emplacement of each bounding boxes provided as parameters.
    :param frame: the original frame where bboxes need to be drawn
    :param bboxes: list of bounding boxes
    :return: the image where the rectangles are drawn
    """
    result = frame.copy()
    result = cv2.cvtColor(result, cv2.COLOR_BGR2RGB)
    for box in bboxes:
        x, y, w, h = box['box']
        cv2.rectangle(result, (x, y), (x + w, y + h), (0, 255, 255), 2)
    return result


def get_face_detection_result(frame_path: str, save_bboxes: bool = True, save_img: bool = True,
                              outdir_path: str = "out_face_detection", face_detector: Callable = detect_faces_mtcnn,
                              **kwargs):
    """
    Run the face_detection pipeline on a single frame.
    :param frame_path: the path to the frame.
    :param save_bboxes: determine if a json file should be saved
    :param save_img: save the resulting image
    :param outdir_path: the directory where the result will be saved
    :param face_detector: the algorithm for detecting faces
    :param kwargs: options for the pipeline
    :key bboxes_outpath : path for the resulting bboxes result
    :key image_outpath : path for the resulting image
    :return: the bounding boxes list
    """
    frame = open_frame(frame_path)
    bboxes = face_detector(frame)
    bboxes = filterDetection(bboxes)
    bboxes_out_path = kwargs.get("bboxes_outpath", frame_path[:-3] + "bboxes.json")
    image_out_path = kwargs.get("image_outpath", frame_path[:-3] + "png")
    if save_bboxes:
        with open(outdir_path + os.path.sep + bboxes_out_path, 'w') as fp:
            json.dump(bboxes, fp, indent=4)

    res_im = draw_bounding_boxes(frame, bboxes)
    if save_img:
        cv2.imwrite(outdir_path + os.path.sep + image_out_path, res_im)
    return bboxes, res_im


def run_mtcnn_on_dir(image_dir: str, output_dir: str, is_numeroted: bool, start_seq: int, end_seq: int) -> None:
    """
    Run the mtcnn pipeline for a whole directory.
    :param image_dir: the directory containing all the images
    :param output_dir: the directory where results needs to be save
    :param is_numeroted: boolean to see if the images are numeroted?
    :param start_seq: starting index for image sequence
    :param end_seq: ending index for image sequence
    :return:
    """
    if is_numeroted:
        frames_path = [f"{image_dir}{os.path.sep}{i:04d}.jpg" for i in range(start_seq, end_seq)]
    else:
        frames_path = [image_dir + os.path.sep + fp for fp in os.listdir(image_dir) if
                       fp[-4:] == ".png" or fp[-4:] == ".jpg" or fp[-5:] == ".jpeg"]

    for frame_path in frames_path:
        _ = get_face_detection_result(frame_path, save_bboxes=True, save_img=True, outdir_path=output_dir)


if __name__ == "__main__":
    bboxes, image = get_face_detection_result("../resources/test/0004.jpg", save_bboxes=False, save_img=False,
                                              face_detector=detect_faces_haar)
    for b in bboxes:
        x, y, w, h = b['box']
        print(f"box at position ({x}, {y}) with dimension {w}x{h} with confidence : {b['confidence']}")

    cv2.imshow("bboxes", image)
    cv2.waitKey()

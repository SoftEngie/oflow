

![EPB](logo_polytech.png)
# Oflow : Motion Segmentation
<pre>
 _   _  _     ______            _     _____  _____   ___    _             _                         _                       
| | | || |    | ___ \          | |   |_   _|/  ___| / _ \  | |           | |                       | |                      
| | | || |    | |_/ /  ______  | |     | |  \ `--. / /_\ \ | |      __ _ | |__    ___   _ __  __ _ | |_  ___   _ __  _   _  
| | | || |    | ___ \ |______| | |     | |   `--. \|  _  | | |     / _` || '_ \  / _ \ | '__|/ _` || __|/ _ \ | '__|| | | | 
| |_| || |____| |_/ /          | |_____| |_ /\__/ /| | | | | |____| (_| || |_) || (_) || |  | (_| || |_| (_) || |   | |_| | 
 \___/ \_____/\____/           \_____/\___/ \____/ \_| |_/ \_____/ \__,_||_.__/  \___/ |_|   \__,_| \__|\___/ |_|    \__, | 
                                                                                                                       /__/ 
</pre>
## Description

This repository contains the code used in the context of my master thesis work. 
The title of the master thesis is : "*Motion analysis for people counting in sequential frames*".
The research questions of this master thesis are the following:
- How can the motion field be analyzed and which type of information can be retrieved
from it?
- How does the motion field compares or complements face detection algorithms?


In this implementation, the method used to analyze the motion field is the optical flow estimation
and ***in fine*** the face detection algorithm used was the Multi Tasks Cascaded Convolutional Network 
(MTCNN for short). 

The code for the project is available in this repository, along with the documentation and mock samples.
Due to privacy and size reasons, the whole dataset is not accessible in this repository. However, it is 
certain that the system should be compatible with others dataset of indoor scenes with consecutive frames.

The repository also contain the manuscript, the source code of the manuscript and the slides of the oral
defense. 

This master thesis was submitted in January 2023. It received a global mark of 12/20.
It should be noted that various advices for improvement have been received during the 
evaluation of this work. The most crucial point of attention have been listed below :

- the number of false positive created by the motion segmentation is way too high and the 
motion segmentation pipeline should be improved.
- the metric used to evaluate motion segmentation is too permissive.
- the system is still to brittle and a refactoring should be considered.


Various leads of improvements have been found but not implemented due to the lack of time :

- Time filters could have been applied as an additional pre-processing.
- The edge extraction pipeline could be further integrated to the motion segmentation
- Constraints on the size and shape of blobs could be applied to reduce the number of false positive

Further informations can be found in the [manuscript](defense/manuscrit.pdf).
## Table of Contents 

- [Prerequisite](#prerequisite)
- [Installation](#installation)
- [Usage](#usage)
- [Credits](#credits)

## Prerequisite
In this work, the RAFT model used for the optical flow estimation is based on the MMFlow library of 
the OpenMMLab group. Therefore, before doing the installation of this repository, you should
make sure that you have installed and followed the instructions of the OpenMMLab group.
These instructions are available in this [repository](https://github.com/open-mmlab/mmflow).

I take this opportunity to thank the OpenMMLab group and their colossal work that have allowed this work.

## Installation
As stated in the [Prerequisite](#prerequisite) section, the MMFlow module should be installed from the 
[repository](https://github.com/open-mmlab/mmflow). The RAFT Model is based on their implementation and, therefore, the oflow 
functions can't function with the RAFT Model.

The code was based in python and is compatible with the
version 3.7 and above.

The different packages needed for the run of the implementation
can be installed via the command : 
<pre>pip install -r requirements.txt</pre>

## Usage

this repository serves mainly as a framework for motion segmentation and also face detection
in indoor scenes. Therefore, different sample code have been written in the different python files.
Those files should be rewritten as wished to adapt to specific scenario.
However, a specific mock example has been written in the intent to serve as an example.


To launch the mock example, you should launch any python files script :

<pre>python src/face_detection.py</pre>

This implementation can produce the following results :

Here is an example of optical flow produced by the RAFT model of the MMflow library
![Optical Flow estimation using the raft model](resources/of_image/0001-0002.png)

Here is an example of motion segmentation based a motion threshold method :
![Motion segmentation](manuscrit-latex/images/lisa_segmented_with_threhsold_parameter.png)

and here is the kind of confusion matrix you can obtain from the implementation :
![Confusion matrix](manuscrit-latex/images/segme_confusion_matrix.png)


## Credits

I would like to thank the LISA Laboratory for their participation in this master thesis. 

Thanks to the OpenMMLab implementation of RAFT model. An optical flow has been computed.
Thanks to [AruniRC](https://github.com/AruniRC), and more specifically to the bbox_iou_evaluation.py that he wrote 
from which my own function has taken great inspiration from. 
